import { Component } from '@angular/core';

import { Platform, Config } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import {
  Plugins,
  StatusBarStyle,
} from '@capacitor/core';

const { StatusBar ,SplashScreen} = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private translate: TranslateService,
    private config: Config
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.show();
      StatusBar.setBackgroundColor({ color: '#3880ff' });
      SplashScreen.hide();
    }).then(() => {
      // TODO : 
      // info about current satus bar with capacitor
      StatusBar.getInfo().then((value) => {
        console.log('StatusBar.getInfo', value)
      })
    });
    this.initTranslate();
  }



  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe((values: any) => {
      this.config.set('backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

}
