
export enum DeviceType {
    Beacon = 'SBeacon',
    Blufi = 'Blufi'
}

export enum AppMode {
    Tester = 1,
    Customer = 2
}

export const Mode : AppMode = AppMode.Customer;
