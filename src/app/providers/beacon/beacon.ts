import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable({providedIn: 'root'})
export class BeaconProvider {

  constructor(public api: Api) { }

  loadBeaconList(siteId: Number) {
    return this.api.get(`bsensors/bysiteid/${siteId}`);
  }

  loadAllBeaconList(siteId:Number){
    return this.api.get(`bsensors/byhierarchyid/${siteId}`);
  }

  UpdateBeacon(siteId: Number, beacon: any) {
    return this.api.put(`bsensors/${beacon.deviceId}/${siteId}`, beacon);
  }

  deleteBeacons(siteId: Number, beaconIDs) {
    return this.api.put(`bsensors/bysiteid/${siteId}`, beaconIDs);
  }

  bulkUploadBeacons(siteId: Number, sid64: Array<any>) {
    return this.api.post(`bsensors/bulkupload/${siteId}`, sid64);
  }

}
