import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class LoadingProvider {

  loader: any;

  constructor(public loadingCtrl: LoadingController) {
    //console.log('Hello LoadingProvider Provider');
  }
  async showLoader(message: string = "Please wait...", duration: number = 3000) {
    if (this.loader && this.loader.instance) {
      //Do Nothing..
    }
    else {
      this.loader = await this.loadingCtrl.create({
        message: message,
        duration: duration
      });

      await this.loader.present();
    }

  }

  async showLoaderWithoutTimer(message: string = "Please wait...") {
    if (this.loader && this.loader.instance) {
      this.setMessage(message);
    }
    else {
      this.loader = await this.loadingCtrl.create({
        message: message
      });

      await this.loader.present();
    }
  }

  async showLoaderWithBackDropWithoutTimer(message: string = "Please wait...") {
    if (this.loader && this.loader.instance) {
      this.setMessage(message);
    }
    else {
      this.loader = await this.loadingCtrl.create({
        message: message,
        // dismissOnPageChange: true,
        showBackdrop: true
      });

      await this.loader.present();
    }
  }

  setMessage(message: string) {
    if (this.loader && this.loader.instance) {
      this.loader.setContent(message);
    }
  }

  hideLoader() {
    try {
      this.loader.dismiss();
    } catch (e) { }
  }

}
