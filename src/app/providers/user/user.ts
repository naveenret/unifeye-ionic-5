import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import { Common } from '../common/common';
import { Auth } from 'aws-amplify';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable({ providedIn: 'root' })
export class User {
  _user: any;

  constructor(public api: Api, private commonService: Common) {

  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    //  let u :any;
    const u = Auth.signIn(accountInfo.username.toLowerCase(), accountInfo.password);
    if (!u) {
      u.then(function (user) {
        Storage.set({
          key: 'token',
          value: user.signInUserSession.accessToken.jwtToken
        })
        this._loggedIn(accountInfo);
      });
    }
    return u;
    /* Auth.signIn(accountInfo.username, accountInfo.password).then((user) => {
      this.storage.set('token', user.signInUserSession.accessToken.jwtToken);
      this._loggedIn(accountInfo);
      return user;    
    }).catch((err) => {
      console.error('ERROR', err);
      return err;
    }); */
    /* let seq = this.api.post('Token/Hashed', { "authInfo": this.commonService.getEncodedPassword(accountInfo.username + ":" + accountInfo.password)}).share();

    seq.subscribe((res: any) => {
      this.storage.set('token', res);
      this._loggedIn(accountInfo);
      /*if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }*/
    /* }, err => {
      console.error('ERROR', err);
    });

    return seq; */
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    // let seq = this.api.post('signup', accountInfo).share();
    let seq = this.api.post('signup', accountInfo).subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  saveLastLocation(sideId: Number) {
    let seq = this.api.put(`user/setlastselectedrtssite?siteId=${sideId}`, sideId);
    seq.subscribe((res: any) => {
      console.log("last selected location saved");
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  getSavedLocationInfo() {

    let seq = this.api.get(`user/info`)

    // seq.subscribe((res: any) => {
    //   console.log(res);
    // }, err => {
    //   console.error('ERROR', err);
    // });
    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(accountInfo) {
    this._user = accountInfo;
  }

  getUserInfo() {
    return this._user;
  }
}
