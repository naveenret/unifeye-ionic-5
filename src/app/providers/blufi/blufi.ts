import { Injectable } from '@angular/core';
import { Api } from '../api/api';


@Injectable({providedIn:'root'})
export class BlufiProvider { 

  constructor(public api: Api) { }

  loadBlufiList(siteId:Number){
    return this.api.get(`bgateways/bysiteid/${siteId}`);
  }

  loadAllBlufiList(siteId:Number){
    return this.api.get(`bgateways/byhierarchyid/${siteId}`);
  }

  updtaeBlufi(siteId: Number, blufi: any) {
    return this.api.put(`bgateways/${siteId}/${blufi.deviceId}`, blufi);
  }

  deleteBlufis(siteId: Number, blufiIDs) {
    return this.api.put(`bgateways/${siteId}`, blufiIDs);
  }

}
