import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Common } from "../common/common";
import { map, catchError, mergeMap } from 'rxjs/operators';
// import { Auth } from 'aws-amplify';
import { of, throwError, Observable, from } from 'rxjs';
import { Auth } from 'aws-amplify';
import { environment } from 'src/environments/environment';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable({ providedIn: 'root' })
export class Api {
  // point this to our wrapper to avoid CORS
  prod: string = 'https://portalapi.digitalcoldchain.com/api';
  qa: string = 'https://u2n0byo4xl.execute-api.us-east-1.amazonaws.com/uat/api';
  dev: string = 'https://u2n0byo4xl.execute-api.us-east-1.amazonaws.com/dev/api';
  url: String = 'https://u2n0byo4xl.execute-api.us-east-1.amazonaws.com/uat/api';

  headers = new HttpHeaders();
  token;
  constructor(public http: HttpClient,
    // public events: Events,
    public commonService: Common) {
    this.getTokenFromStorage();
  }

  setUrlToProd() {
    this.url = this.prod;
  }

  setUrlToQa() {
    this.url = this.qa;
  }

  setUrlToDev() {
    this.url = this.dev;
  }

  getUrl() {
    return this.url;
  }

  getProdUrl() {
    return this.prod;
  }

  getQaUrl() {
    return this.qa;
  }

  getDevUrl() {
    return this.dev;
  }

  getTokenFromStorage() {
    Storage.get({key: 'token'}).then((val) => {
      console.log('###TOKEN API SERVICE###' + val)
      this.token = val;
      // return this.token;
    });
  }

  appendHeaders(headers: HttpHeaders) {
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + this.commonService.getTokenFromStorage());
  }

  postLogin(endpoint: string, body: any) {

    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post(this.url + '/' + endpoint, body, { headers: headers });
  }

  get(endpoint: string, params?: any, reqOpts?: any, appendToken?: boolean): Observable<any> {

    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return from(Auth.currentSession()).pipe(mergeMap((session) => {
      console.log(session);
      
    // let session: any = {};
    let headers: any = {};
    if (session.getAccessToken().getJwtToken() !== "") {
      headers = new HttpHeaders({ 'Authorization': session.getAccessToken().getJwtToken() });
    }
    return this.http.get(environment._API_URL + '/' + endpoint, { headers: headers })
      .pipe(
        map(res => res),
        catchError(err => throwError(err))
      )

  }));
}

  



  /* if (this.commonService.getTokenFromStorage() !== "") {
     const headers = new HttpHeaders({ 'Authorization':  this.commonService.getTokenFromStorage() });
     return this.http.get(this.url + '/' + endpoint, { headers: headers }).catch((error) => {
 
       if(error.status == 401 || error.status == 400){
         this.events.publish('session:expired');
       }
       throw(error);
     });
   }
   else {
     return this.http.get(this.url + '/' + endpoint).catch((error) => {
 
       if(error.status == 401 || error.status == 400){
         this.events.publish('session:expired');
       }
       throw(error);
     });
   } */

  post(endpoint: string, body: any, reqOpts?: any, appendToken?: boolean) {

    console.log('###API####' + this.url + '/' + endpoint + '###' + body);

    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // return of(Auth.currentSession()).pipe(mergeMap((session: any) => {
    return from(Auth.currentSession()).pipe(mergeMap((session: any) => {

      if (session.getAccessToken().getJwtToken() !== "") {
        const headers = new HttpHeaders({ 'Authorization': session.getAccessToken().getJwtToken() });
        return this.http.post(this.url + '/' + endpoint, body, { headers: headers })
          .pipe(
            map(res => res),
            catchError(err => throwError(err))
          )
        // .catch((error) => {
        //   if (error.status == 401 || error.status == 400) {
        //     this.events.publish('session:expired');
        //   }
        //   throw (error);
        // });
      }
      else {
        return this.http.post(this.url + '/' + endpoint, body)
          .pipe(
            map(res => res),
            catchError(err => throwError(err))
          )
        // .catch((error) => {

        //   if (error.status == 401 || error.status == 400) {
        //     this.events.publish('session:expired');
        //   }
        //   throw (error);
        // });

      }
    }));

    /* if (this.commonService.getTokenFromStorage() !== "") {
      const headers = new HttpHeaders({ 'Authorization':  this.commonService.getTokenFromStorage() });
      return this.http.post(this.url + '/' + endpoint, body, { headers: headers }).catch((error) => {
  
        if(error.status == 401 || error.status == 400){
          this.events.publish('session:expired');
        }
        throw(error);
      });
    }
    else {
      return this.http.post(this.url + '/' + endpoint, body).catch((error) => {
  
        if(error.status == 401 || error.status == 400){
          this.events.publish('session:expired');
        }
        throw(error);
      });
    } */
  }

  put(endpoint: string, body: any, reqOpts?: any, appendToken?: boolean) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // return of(Auth.currentSession()).pipe(mergeMap((session: any) => {
    return from(Auth.currentSession()).pipe(mergeMap((session: any) => {

      if (session.getAccessToken().getJwtToken() !== "") {
        const headers = new HttpHeaders({ 'Authorization': session.getAccessToken().getJwtToken() });
        return this.http.put(this.url + '/' + endpoint, body, { headers: headers })
          .pipe(
            map(res => res),
            catchError(err => throwError(err))
          )
        // .catch((error) => {

        //   if (error.status == 401 || error.status == 400) {
        //     this.events.publish('session:expired');
        //   }
        //   throw (error);
        // });
      }
      else {
        return this.http.put(this.url + '/' + endpoint, body)
          .pipe(
            map(res => res),
            catchError(err => throwError(err))
          )
        // .catch((error) => {

        //   if (error.status == 401 || error.status == 400) {
        //     this.events.publish('session:expired');
        //   }
        //   throw (error);
        // });

      }
    }));

    /* if (this.commonService.getTokenFromStorage() !== "") {
      const headers = new HttpHeaders({ 'Authorization':  this.commonService.getTokenFromStorage() });
      return this.http.put(this.url + '/' + endpoint, body, { headers: headers }).catch((error) => {
  
        if(error.status == 401 || error.status == 400){
          this.events.publish('session:expired');
        }
        throw(error);
      });
    }
    else {
      return this.http.put(this.url + '/' + endpoint, body, reqOpts).catch((error) => {
  
        if(error.status == 401 || error.status == 400){
          this.events.publish('session:expired');
        }
        throw(error);
      });
    } */
  }

  delete(endpoint: string, reqOpts?: any, appendToken?: boolean) {
    /*if(appendToken) {
      reqOpts.headers.append('Authorization', 'Bearer ' + this.commonService.getTokenFromStorage()); // fix this later
    }*/
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any, appendToken?: boolean) {
    /*if(appendToken) {
      reqOpts.headers.append('Authorization', 'Bearer ' + this.commonService.getTokenFromStorage()); // fix this later
    }*/
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
}
