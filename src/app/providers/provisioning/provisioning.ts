import { Injectable, NgZone } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastProvider } from '../toast/toast';
import { Common } from '../common/common';
import { LoadingProvider } from '../loading/loading';
import { Beacon } from '../../models/Beacon';
import { Blufi } from '../../models/Blufi';
import { Template } from '../../models/Template';
import { TranslateService } from '@ngx-translate/core';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { DeviceType } from 'src/app/@constants/app.constant';

declare var cordova: any;

@Injectable({providedIn: 'root'})
export class ProvisioningProvider {

  //#region PRIVATE MEMBERS

  private navCtrl: NavController;

  private deviceType: DeviceType;

  private deviceFoundCallback: any;

  private deviceLostCallback: any;

  private templateLoadedCallback: any;

  private scanTimer: any;

  private pauseSubscription:any;

  //#endregion

  //#region CONSTRUCTOR

  constructor(private diagnostic: Diagnostic,
    private common: Common,
    public platform: Platform,
    private LoadingProvider: LoadingProvider,
    public translateService: TranslateService,
    private ngZone: NgZone,
    public alertCtrl: AlertController,
    private toastProvider: ToastProvider) {

      this.platform.ready().then(() => {
        let self = this;
        this.pauseSubscription = this.platform.pause.subscribe(() => {
          console.log('application paused stopping scan');
          self.stopScanning();
        });
      });

    if (this.platform.is('android')) {
      //register the bluetooth state change handler.
      this.registerBluetoothStatehandler();
    }
  }

  ionViewWillUnload() {
    this.pauseSubscription.unsubscribe();
  }

  //#endregion

  //#region PUBLIC METHODS

  public setNav(nav) {
    this.navCtrl = nav;
  }

  public stopScan() {
    cordova.plugins.BluProvisionWrapper.stopScan();
  }

  //start the provisioning process depending on the device type.
  public startProvisioning(deviceType: DeviceType, deviceFoundCallback, deviceLostCallback): void {

    this.deviceFoundCallback = deviceFoundCallback;
    this.deviceLostCallback = deviceLostCallback;
    this.deviceType = deviceType;

    if (this.platform.is('android')) {

      //check for the bluetooth permission
      this.checkForBluetoothPermission();
    }
    else {
      //start provisionig process
      this.requestIOSBluetoothPermission();
    }
  }

  public stopScanning(){
    this.LoadingProvider.hideLoader();
    cordova.plugins.BluProvisionWrapper.stopScan();
    // this.toastProvider.present('Scanning stopped');
  }

  public requestIOSBluetoothPermission(){
    let self = this;
    cordova.plugins.BluProvisionWrapper.bluetoothPermission("",
    (msg) => {

      this.ngZone.run(() => {
        //handle the bluetooth permission success.
        self.requestIOSLocationPermission();
      });
    },
    (err) => {

      this.ngZone.run(async () => {
        //handle the bluetooth permission error.
        const prompt =await this.alertCtrl.create({
          header: 'Permission Denied',
          subHeader: "Bluetooth permission denied, please enable bluetooth permission from settings.",
          backdropDismiss: false,
          buttons: [
            {
              text: this.translateService.instant('OK'),
              handler: data => {
                //Do Nothing...
              }
            }
          ]
        });
        await prompt.present();
      });
    });
  }

  public requestIOSLocationPermission(){
    let self = this;
    cordova.plugins.BluProvisionWrapper.locationPermission("",
    (msg) => {
      this.ngZone.run(() => {
        //handle the location permission success.
        self.startProvisioningProcess();
      });
    },
    (err) => {
      this.ngZone.run(async () => {
        //handle the locatrion permission error.
        const prompt =await this.alertCtrl.create({
          header: 'Permission Denied',
          subHeader: err.code,
          backdropDismiss: false,
          buttons: [
            {
              text: this.translateService.instant('OK'),
              handler: data => {
                //Do Nothing...
              }
            }
          ]
        });
        await prompt.present();
      });
    });
  }

  public provisionDevice(param) {

    let self = this;
    //show the loading message.
    self.LoadingProvider.showLoaderWithBackDropWithoutTimer("Start Provisioning..");

    //provision the beacon.
    cordova.plugins.BluProvisionWrapper.provision(param, (msg) => {
      
      this.ngZone.run(() => {
        //handle the provision success event.
        self.handleProvisionSuccessHandler(self, msg);
      });
    }, (err) => {

      this.ngZone.run(() => {
        //handle the provision error event.
        self.handleProvisionErrorHandler(self, err);
      });
    });
  }

  public loadTemplates(param, templateLoadedCallback) {

    this.templateLoadedCallback = templateLoadedCallback;

    let self = this;

    //show the loader with Loading templates message.
    self.LoadingProvider.showLoaderWithoutTimer("Loading templates...");

    //user has selected a beacon for provisoning hence scanning is not required
    cordova.plugins.BluProvisionWrapper.stopScan();

    //Get the beacon templates from the plugin.
    cordova.plugins.BluProvisionWrapper.getTemplate(param, (msg) => {

      this.ngZone.run(() => {
        //handle the template loaded event.
        self.handleTemplateLoadedEvent(self, msg.templates);
      });

    }, (err) => {

      this.ngZone.run(() => {
        if (err.Template_Fail){
          self.showErrorAlert(self, err.Template_Fail)
        }else {
          self.showErrorAlert(self, JSON.stringify(err))
        }

        //handle template load error event
        self.handleTemplateLoadErrorEvent(self, err);
      });
    });
  }

  private showErrorAlert(self, err) {

    const confirm = self.alertCtrl.create({
      title: 'Error',
      message: err,
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            self.navCtrl.setRoot('BluProvisionPage', { isFromProvision: true });
            self.navCtrl.parent.select(0);
          }
        }
      ]
    });
    confirm.present();
  }


  //#endregion

  //#region PRIVATE METHODS

  //register bluetooth state handler.
  private registerBluetoothStatehandler() {
    var self = this;

    //register bluetooth state change handler.
    this.diagnostic.registerBluetoothStateChangeHandler(function (state) {

      //if bluetooth is powered on..
      if (state.toString() === cordova.plugins.diagnostic.bluetoothState.POWERED_ON.toString()) {

        //check for a location permission.
        self.checkForLocationPermission()
      }
    });
  }

  private checkForBluetoothPermission() {

    //check if bluetooth is available on the device.
    if (!this.diagnostic.isBluetoothAvailable()) {
      this.toastProvider.present('Bluetooth is not available on this device');
    }
    else {

      if (this.platform.is('ios')) {

        this.diagnostic.requestBluetoothAuthorization();
      }
      else {

        //check if bluetooth is enabled or not.
        return this.diagnostic.isBluetoothEnabled().then(enabled => {

          //if bluetooth is turned off.
          if (!enabled) {

            //turn on the bluetooth.
            this.diagnostic.setBluetoothState(true);
          }
          else {

            //check for a location permission.
            this.checkForLocationPermission();
          }
        }).catch(rejected => {
          this.toastProvider.present("Error occured while bluetooth" + rejected);
        });
      }
    }
  }

  //check for a location permission.
  private checkForLocationPermission() {

    //check if the location is not available on a device.
    if (!this.diagnostic.isLocationAvailable()) {
      this.toastProvider.present('Location is not available on this device');
    }
    else {

      //if location permission is enabled or not.
      this.diagnostic.isLocationAuthorized().then(enabled => {

        //if location permission is not enabled..
        if (!enabled) {

          //ask/request for a location permission.
          this.diagnostic.requestLocationAuthorization().then(status => {

            switch (status) {
              case this.diagnostic.permissionStatus.NOT_REQUESTED:
                this.toastProvider.present("Permission not requested");
                break;
              case this.diagnostic.permissionStatus.GRANTED:

                //if location permission granted then start provisionig process
                this.startProvisioningProcess()
                break;
              case this.diagnostic.permissionStatus.DENIED:
                this.toastProvider.present("Permission denied");
                break;
              case this.diagnostic.permissionStatus.DENIED_ALWAYS:
                this.toastProvider.present("Permission permanently denied");
                break;
            }
          });
        }
        else {

          //start provisioning process.
          this.startProvisioningProcess();
        }
      }).catch(rejected => {
        this.toastProvider.present("Error occured while Location" + rejected);
      });
    }
  }

  //start provisioning process.
  private startProvisioningProcess() {

    // Initialize the blu-vision plugin for beacon.
    cordova.plugins.BluProvisionWrapper.init(this.deviceType);

    //Sign in into the plugin.
    this.signIn();

  }

  //signin into the blu plugin.
  private signIn() {

    //store the current page instance reference
    let self = this;

    //show the loader with signing in message.
    self.LoadingProvider.showLoaderWithoutTimer("Signing in...");

    //get the project token.
    let token = this.common.getProjectTokenFromStorage();

    //sign in into the blu plugin with the project token.
    cordova.plugins.BluProvisionWrapper.signIn(token,
      (msg) => {

        this.ngZone.run(() => {

          let code = msg.code;

          if (code == "SignIn_Success") {
            self.handleSignInSuccessEvent(self);
            self.startScanTimer();
          }
          else if (code == "Beacon_Found") {
            this.LoadingProvider.hideLoader();
            clearTimeout(this.scanTimer);
            self.handleBeaconFoundEvent(self, msg);
          }
          else if (code == "Beacon_Lost") {
            self.handleBeaconLostEvent(self, msg);
          }
        });
      },
      (err) => {
        self.handleSignInError(self, err);
      });
  }

  private startScanTimer(){
    this.scanTimer = setTimeout(this.stopScanOnTimeout.bind(this), 50000);
  }

  private stopScanOnTimeout(){
    this.LoadingProvider.hideLoader();
    cordova.plugins.BluProvisionWrapper.stopScan()
    clearTimeout(this.scanTimer);
    this.navCtrl.navigateRoot('tabs');
  }

  private handleBeaconFoundEvent(self, msg) {
    //this.LoadingProvider.hideLoader();

    if (msg.type == DeviceType.Beacon) {

      let beacon = new Beacon();
      beacon.name = msg.name;
      beacon.address = msg.address;
      beacon.type = msg.type;
      beacon.id = msg.id;
      beacon.idhex = msg.hex;
      let quality = 0;
      if(msg.rssi <= -100){
        quality = 0;
      }else if(msg.rssi >= -50){
        quality = 100;
      }else{
        quality = 2 * (msg.rssi + 100);
      }
      beacon.rssi = quality;

      self.deviceFoundCallback(beacon);
    }
    else if (msg.type == DeviceType.Blufi) {
      let blufi = new Blufi();
      blufi.name = msg.name;
      blufi.address = msg.address;
      blufi.type = msg.type;
      blufi.id = msg.id;
      blufi.idhex = msg.hex;
      let quality = 0;
      if(msg.rssi <= -100){
        quality = 0;
      }else if(msg.rssi >= -50){
        quality = 100;
      }else{
        quality = 2 * (msg.rssi + 100);
      }
      blufi.rssi = quality;

      self.deviceFoundCallback(blufi);
    }
  }

  private handleSignInSuccessEvent(self) {

    if (self.deviceType === DeviceType.Beacon) {
      //set the loader message to scanning sensors.
      self.LoadingProvider.setMessage('Scanning Transmitter');
    }
    else {
      //set the loader message to scanning sensors.
      self.LoadingProvider.setMessage('Scanning Gateways');
    }
  }

  private handleBeaconLostEvent(self, msg) {

    this.startScanTimer();
    this.LoadingProvider.hideLoader();
    //show the loading message according to the device type.
    if (self.deviceType === DeviceType.Beacon) {
      this.LoadingProvider.showLoaderWithBackDropWithoutTimer('Transmitter lost scanning...');
    }
    else {
      this.LoadingProvider.showLoaderWithBackDropWithoutTimer('Gateway lost scanning...');
    }

    //fire the device lost callback
    this.deviceLostCallback(msg.id.toString());
  }

  private handleSignInError(self, err) {

    //Hide the loader.
    self.LoadingProvider.hideLoader();

    if (err.Fail == "Bluetooth") {

      //display error message.
      self.toastProvider.present("Turn on Bluetooth");
    } else if (err.Fail == "Location") {

      //display error message.
      self.toastProvider.present("Location permission not enabled");

    } else if (err.Fail == "SignIn_Failed") {

      //display error message.
      self.toastProvider.present("Sign In failed");
    }
  }

  //handle the template loaded event.
  private handleTemplateLoadedEvent(self, templates) {

    var retVal = new Array<Template>();

    //populate the local template array to update the UI.
    for (var template of templates) {
      let temp = new Template();
      temp.id = template.templateId;
      temp.name = template.name;
      retVal.push(temp);
    }

    //show the loader with Loading templates message.
    self.LoadingProvider.hideLoader();

    self.templateLoadedCallback(retVal);

  }

  //handle the template load error event.
  private handleTemplateLoadErrorEvent(self, err) {

    //hide the loader.
    self.LoadingProvider.hideLoader();

    if (err.Template_Fail == "Template_Fail") {

      //display error message.
      self.toastProvider.present("Failed to load template");
    }
  }

  //handle the provision error event.
  public async handleProvisionErrorHandler(self, err) {

    //hide the loader.
    self.LoadingProvider.hideLoader();

    //format the error messages.
    this.formatErrorMessages(err);

    //show the alert with the error message.
    const prompt =await this.alertCtrl.create({
      header: 'Provisioning failed',
      subHeader: err.reason,
      backdropDismiss: false,
      buttons: [
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            self.navCtrl.setRoot('BluProvisionPage', { isFromProvision: true });
            self.navCtrl.parent.select(0);
          }
        }
      ]
    });
    await prompt.present();
  }

  //handle the provision success event.
  public handleProvisionSuccessHandler(self, msg) {

    if (msg.code == "Provision_Sucess") {

      //hide loader.
      self.LoadingProvider.hideLoader();

      //sign out and stop scan from the plugin.
      cordova.plugins.BluProvisionWrapper.stopScan();

      //show success alert.
      self.showConfirmModel(self);

      //self.navCtrl.setRoot('BluProvisionPage', { isFromProvision: true });

    }
    else if (msg.code == "Status_Changed") {

      //change the loading message.
      self.LoadingProvider.setMessage(msg.result);
    }
    else if (msg.code == "Desc_Changed") {

      //show message.
      self.LoadingProvider.setMessage(msg.result);
    }
  }

  public async showConfirmModel(self) {

    const confirm =await self.alertCtrl.create({
      header: 'Provision Success.',
      message: 'Device provisioned successfully. Do you want to provison more devices?',
      backdropDismiss: false,
      buttons: [
        {
          text: 'No',
          handler: () => {
            self.navCtrl.setRoot('BluProvisionPage', { isFromProvision: true });
            self.navCtrl.parent.select(0);
          }
        },
        {
          text: 'Yes',
          handler: () => {
            if (self.deviceType === DeviceType.Beacon) {
              self.navCtrl.navigateForward('BluBeaconProvisionPage');
            }
            else {
              self.navCtrl.navigateForward('BluBlufiProvisionPage');
            }
          }
        }
      ]
    });

    await confirm.present();
  }

  public formatErrorMessages(err) {

    if (err.result == "ERROR") {

      err.reason = err.reason.replace(/Bluzone/gi, 'Emerson');
      err.reason = err.reason.replace(/bluvision/gi, 'Emerson');
      err.reason = err.reason.replace(/Beacon/gi, 'Transmitter');
      err.reason = err.reason.replace(/Blufi/gi, 'Gateway');
      err.reason = err.reason.replace(/Project/gi, 'Location');

      err.reason = err.reason.replace(/\n\n/g, '</p><p>');
      err.reason = err.reason.replace(/\n\t/g, '</p><p>');
    }
  }

  //#endregion

}
