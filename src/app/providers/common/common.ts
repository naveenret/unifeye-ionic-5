import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
// import { Plugins } from '@capacitor/core';

// const { Storage } = Plugins;


@Injectable({ providedIn: 'root' })
export class Common {

  token: string;
  projectToken: string;
  isAllLocationSelected = false;

  constructor() { }

  // getHahedPassword(userName: string, passWord: string) {
  //   let hash = CryptoJS.SHA256(userName.concat(passWord));
  //   let hash_Base64 = hash.toString(CryptoJS.enc.Base64);
  //   return hash_Base64;
  // }

  // getEncodedPassword(password: string) {
  //   return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(password));
  // }

  setTokenToLocalStorage(token) {
    this.token = token;
    // return Storage.set({
    //   key: 'token',
    //   value: token
    // })
  }

  setProjectTokenToLocalStorage(token) {
    this.projectToken = token;
    // return Storage.set({
    //   key: 'projectToken',
    //   value: token
    // })  
  }

    getProjectTokenFromStorage() {
      return this.projectToken;
      // return this.storage.get('projectToken').then((val) => {
      //   return val;
      // });
    }

    getTokenFromStorage() {

      if (this.token) {
        return this.token;
      }
      else {
        return "";
      }

      // alert('getTokenFromStorage called');
      // return this.storage.get('token').then((val) => {
      //   alert('Token' + val);
      //   console.log('###TOKEN COMMON SERVICE###' + val)
      //   return val;
      // }, (error) => {
      //   alert('error from getTokenFromStorage' + error);
      // });
    }

    setAllLocationSelected(value) {
      this.isAllLocationSelected = value;
    }

    getAllLocationSelected() {
      return this.isAllLocationSelected;
    }

  }
