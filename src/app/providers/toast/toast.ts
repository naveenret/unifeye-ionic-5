import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class ToastProvider {

  constructor(public toastCtrl: ToastController) {
  }

  async present(message: string, duration: number = 3000) {

     
    let toast = await this.toastCtrl.create({
      message: message,
      duration: duration,
      position: 'top'
    });
    await toast.present();
  }

}
