import { Injectable, NgZone } from '@angular/core';
import { ToastProvider } from '../toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { Platform, AlertController } from '@ionic/angular';

// import 'capacitor-secure-storage-plugin';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({ providedIn: 'root' })
export class AutoLoginProvider {
  private loginCallback: any;
  constructor(public platform: Platform,
    private ngZone: NgZone,
    public alertCtrl: AlertController,
    public translateService: TranslateService,
  ) {
  }


  saveCredentials(accountInfo: any) {
    console.log('Storage save with acccount inof', accountInfo);
    const key = accountInfo.username;
    const value = accountInfo.password;

    Storage.set({ key, value }).then((sucess) => {
      console.log('save credential success', sucess)
      // localStorage.setItem('KEYS', btoa(key));

    });
    // Secure storage copy 
    // SecureStoragePlugin.set({ key, value }).then((sucess) => {
    //   console.log('save credential success', sucess)
    //   // localStorage.setItem('KEYS', btoa(key));
    //   Storage.set({
    //     key: 'KEYS',
    //     value: btoa(key)
    //   })
    // });

  }

  getCredentials(userName: string) {
    let self = this;
    console.log('Storage :: get credential by username', userName);

    const key = userName;
    Storage.get({ key }).then((value: any) => {
      console.log('value', value, userName)
      self.ngZone.run(() => {
        if (value && value.value)
          self.loginCallback(userName, value.value);
      });
    }).catch(err => {
      console.log('err', err);
    })
    // SecureStoragePlugin.get({ key }).then((value: any) => {
    //   self.ngZone.run(() => {
    //     self.loginCallback(userName, value);
    //   });
    // }).catch(err => {
    //   console.log('err', err);
    // })
  }

  attemptAutoLogin(loginCallback: any) {
    this.loginCallback = loginCallback;
    let self = this;
    this.platform.ready().then(() => {


      console.log('Storage :: attems to login call back');
      Storage.keys().then((value: any) => {
        console.log('Storage :: keys', value);
        console.log('Storage :: keys[0]', value.keys[0]);
        if (value && value.keys && value.keys[0]) {
          console.log('yet to called get credential')
          self.getCredentials(value.keys[0]);
        } else {
          console.log('no keys found ')
        }
      }, (err) => {
        console.log("Error, KEYS not found in localstorage", err);

      }).catch((err) => {
        console.log("Error, KEYS not found in localstorage catch err", err);
      })
    });
  }

  clearAllCredentials(logoutCallback: any, logoutErrorCallback: any) {
    console.log('Storage :: cleaer all crendentail');
    let self = this;

    Storage.clear().then(() => {
      console.log('storage clear success status is ', status);
      self.ngZone.run(() => {
        logoutCallback();
      });
    }).catch(() => {
      console.log('storage clear error status is ', status)
      self.ngZone.run(() => {
        logoutErrorCallback();
      });
    })
    // SecureStoragePlugin.clear().then(status => {
    //   if (status) {
    //     console.log('storage clear success status is ', status);
    //     self.ngZone.run(() => {
    //       logoutCallback();
    //     });
    //   } else {
    //     console.log('storage clear error status is ', status)
    //     self.ngZone.run(() => {
    //       logoutErrorCallback();
    //     });
    //   }
    // })


  }

  clearCredentials() {
    console.log('Storage :: clear credential');
    let self = this;
    Storage.clear().then(() => {
      console.log('storage clear success status is ', status);
      self.ngZone.run(() => { });
    }).catch(() => {
      console.log('storage clear error status is ', status)
      self.ngZone.run(() => { });
    })
    // SecureStoragePlugin.clear().then(status => {
    //   if (status) {
    //     console.log('storage clear success status is ', status);
    //     localStorage.removeItem('KEYS');
    //     self.ngZone.run(() => { });
    //   } else {
    //     console.log('storage clear error status is ', status)
    //     localStorage.removeItem('KEYS'); // TODO : if we dont need it remove it later 
    //     self.ngZone.run(() => { });
    //   }
    // })
  }
}
