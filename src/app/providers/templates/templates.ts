import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import { Observable } from 'rxjs/Observable';
import { GateWayTemplate } from '../../models/Template';
import { of } from 'rxjs';

@Injectable({providedIn: 'root'})
export class TemplateProvider {


  constructor(public api: Api) { }

  getTemplates(siteId: Number) {
    return this.api.get(`btemplates/bysiteid/${siteId}`);
  }

  getGatewayTemplates(projectId:Number){
    return this.api.get(`locations/${projectId}/Provisioning/BGatewayTemplates`);
  }

  addGatewayTemplate(formdata) {
    return this.api.post(`btemplates/bgatewaytemplate`, formdata); // blu API : POST /papis/v1/projects/{projectId}/blufiTemplates
  }

  getGatewayTemplate(templateID: Number, siteID: Number) {

    if (templateID === 0) {
      
      return of(new GateWayTemplate());
    }
    else {
     return this.api.get(`btemplates/bgatewaytemplate/${templateID}?siteId=${siteID}`);
    }
  }

  updateGatewayTemplate(templateObj) {
    return this.api.put(`btemplates/bgatewaytemplate`, templateObj);
  }

  deleteGatewayTemplate(templateID, siteID) {
    return this.api.put(`btemplates/bgatewaytemplate/delete?siteId=${siteID}`,[templateID]);
  }

  addBeaconTemplate(projectId:Number, formdata) {
    return this.api.post(`beacontemplate`, formdata); // blu API :
  }
  
}
