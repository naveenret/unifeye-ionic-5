import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable({providedIn: 'root'})
export class AlertProvider {

  constructor(public api: Api) { }

  loadAlertList(siteId: Number) {
    return this.api.get(`alarms/bysiteid/${siteId}`);
  }

  loadAllAlertList(siteId:Number){
    return this.api.get(`alarms/byhierarchyid/${siteId}`);
  }

}
