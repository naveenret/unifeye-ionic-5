import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable({providedIn: 'root'})
export class ProjectProvider {

  selectedSite: any = undefined

  constructor(public api: Api) { }

  loadProjects(){
    return this.api.get('haccp/site');
  }

  loadProjectOverview(siteId:Number) {
    return this.api.get(`bdashboard/bysiteid/${siteId}`);
  }

  loadAllDashboardOverview(siteId:Number) {
    return this.api.get(`bdashboard/byhierarchyid/${siteId}`);
  }

  getProjectToken(projectId: Number) {
    return this.api.get(`haccp/site/${projectId}/token`);
  }

  getSiteId() {
    if (this.selectedSite !== undefined) {
      return this.selectedSite.siteId;
    }
    else {
      return -1;
    }
  }

  getProjectId() {
    if (this.selectedSite !== undefined) {
      return this.selectedSite.projectId;
    }
    else {
      return 0;
    }
  }

  setSite(site) {
    this.selectedSite = site;
  }

  getSite() {
    return this.selectedSite;
  }


}
