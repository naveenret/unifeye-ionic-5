
export class Template {

  public name: string = "";
  public id: string = "";

}

export class GateWayTemplate {

  templateId: number;
  name: string;
  ssid: string;
  securityType: any;
  passphrase: string;
  description: string;

  constructor() {

      this.templateId = 0;
      this.name = '';
      this.ssid = '';
      this.securityType = {
        securityTypeKey: 'WEP',
        enterprise: true
      };
      this.passphrase = '';
      this.description = '';

  }

}