import { Template } from "./Template";
import { Beacon } from "./Beacon";
import { Blufi } from "./Blufi";

export class ProvisionInfo {

  public beacon: Beacon;
  public template: Template;
}

export class BlufiProvisionInfo {

  public blufi: Blufi;
  public template: Template;
}

