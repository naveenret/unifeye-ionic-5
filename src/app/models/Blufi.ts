
export class Blufi {

  public name: string = "";
  public id: number = 0;
  public idhex: string = "";
  public address: string = "";
  public type: string = "";
  public rssi: number = 0;
}
