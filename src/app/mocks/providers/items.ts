import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable({providedIn: 'root'})
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Default device",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "This is device 1",
  };


  constructor() {
    let items = [
      {
        "name": "Device 1",
        "profilePic": "assets/img/speakers/bear.jpg",
        "about": "This is device 2 description"
      },
      {
        "name": "Device 2",
        "profilePic": "assets/img/speakers/cheetah.jpg",
        "about": "This is device 3 description"
      },
      {
        "name": "Device 3",
        "profilePic": "assets/img/speakers/duck.jpg",
        "about": "This is device 4 description"
      },
      {
        "name": "Device 4",
        "profilePic": "assets/img/speakers/eagle.jpg",
        "about": "This is device 5 description"
      },
      {
        "name": "Device 5",
        "profilePic": "assets/img/speakers/elephant.jpg",
        "about": "This is device 6 description"
      },
      {
        "name": "Device 6",
        "profilePic": "assets/img/speakers/mouse.jpg",
        "about": "This is device 7 description"
      },
      {
        "name": "Device 7",
        "profilePic": "assets/img/speakers/puppy.jpg",
        "about": "This is device 8 description"
      }
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
