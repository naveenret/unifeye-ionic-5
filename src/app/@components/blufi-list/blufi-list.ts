import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'blufi-list',
  templateUrl: 'blufi-list.html'
})
export class BlufiListComponent {

  @Input()
  list: Array<any>;

  @Output()
  blufiSelected: EventEmitter<any> = new EventEmitter();


  constructor() {
    
  }

  onBlufiClick(item: any) {
    this.blufiSelected.emit(item);
  }

}
