import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DeviceTypeSelectionModalComponent } from './device-type-selection-modal/device-type-selection-modal';
import { TranslateModule } from '@ngx-translate/core';
import { BeaconListComponent } from './beacon-list/beacon-list';
import { BlufiListComponent } from './blufi-list/blufi-list';
import { TemplateListComponent } from './template-list/template-list';
import { BluTemplateFormComponent } from './blu-template-form/blu-template-form';
import { CalibrationModeComponent } from './calibration-mode/calibration-mode';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

 
@NgModule({
	declarations: [
		DeviceTypeSelectionModalComponent,
		BeaconListComponent,
		BlufiListComponent,
		TemplateListComponent,
		BluTemplateFormComponent,
		CalibrationModeComponent
	],
	imports: [
		IonicModule,
		CommonModule,
		FormsModule,
		TranslateModule.forChild(),
		ReactiveFormsModule
	],
	schemas:[CUSTOM_ELEMENTS_SCHEMA],
	exports: [
		DeviceTypeSelectionModalComponent,
		BeaconListComponent,
		BlufiListComponent,
		TemplateListComponent,
		BluTemplateFormComponent,
		CalibrationModeComponent
	]
})
export class ComponentsModule { }
