//#region IMPORT
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { debounceTime } from 'rxjs/operators';
import { GenericValidator } from '../../validators/generic-validator'
import { ModalController } from '@ionic/angular';
import { TemplateProvider } from 'src/app/providers/templates/templates';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { ProjectProvider } from 'src/app/providers/project/project';
import { User } from 'src/app/providers/user/user';
//#endregion

@Component({
  selector: 'blu-template-form',
  templateUrl: 'blu-template-form.html'
})
export class BluTemplateFormComponent {

  //#region PUBLIC MEMBERS

  @Input()
  templateId: number;

  gatewayTemplateInfo: any;

  gatewayTemplateForm: FormGroup;

  siteId: number;

  //Represents the validation error messages for controls
  displayMessage: { [key: string]: string } = {};

  //#endregion

  //#region PRIVATE MEMBERS

  //Performs the validation for the form.
  private genericValidator: GenericValidator;
  public update: boolean;
  //#endregion

  //#region CONSTRUCTOR
  constructor(public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public templateProvider: TemplateProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public projectProvider: ProjectProvider,
    public translateService: TranslateService,
    public user: User) {
    this.update = false;
    this.siteId = this.projectProvider.getSiteId();
    if (this.siteId != -1) {
      this.user.saveLastLocation(this.siteId).subscribe((data: any) => {
        console.log("save success " + data);
      }, (err) => {
        console.error("save failed");
      });
    }
  }

  setUpdate($event) {
    if ($event._value !== '') {
      this.update = true;
    }
  }

  //#endregion

  //#region INITIALIZATION
  ngOnInit(): void {

    this.LoadingProvider.showLoader();

    this.templateProvider.getGatewayTemplate(this.templateId, this.siteId).subscribe((data) => {

      this.gatewayTemplateInfo = data;

      this.initializeTemplateForm();

      //hide the loader.
      this.LoadingProvider.hideLoader();

    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
      else {
        this.modalCtrl.dismiss(false);
        // this.viewCtrl.dismiss(false);
      }
    });
  }

  //#endregion

  //#region PUBLIC METHODS

  onSecurityTypeChanged() {

    this.update = true;
    if (this.gatewayTemplateForm.value.security === 'OPEN') {

      this.gatewayTemplateForm.controls['passphrase'].setValue('');

      this.gatewayTemplateForm.get('passphrase').clearValidators();

      this.gatewayTemplateForm.get('passphrase').updateValueAndValidity();

    }
    else {

      this.gatewayTemplateForm.controls['passphrase'].setValidators([Validators.required]);

      this.gatewayTemplateForm.get('passphrase').updateValueAndValidity();
    }
  }

  submitgatewayTemplateForm() {

    if (this.gatewayTemplateForm.valid) {
      if (this.templateId === 0) {
        this.createGateway();
      }
      else {
        this.updateGateway();
      }
    }
  }

  //#endregion

  //#region PRIVATE METHODS

  private createGateway() {

    this.LoadingProvider.showLoader();

    var template = {
      "tags": [],
      "securityType": {
        "securityTypeKey": this.gatewayTemplateForm.value.security,
        "enterprise": this.gatewayTemplateForm.value.security === "Enterprise"
      },
      // gatewayTemplateForm.security
      // "verifyCert": true,
      // "rotationInterval": 60,
      // "udpConfigurationType": "DISABLED",
      // "udpHost": null,
      // "udpPort": 0,
      // "autoCreateGeofencePolicy": false,
      // "autoCreatePLSPolicy": false,
      // "netMask": "255.255.255.0",
      // "geofenceWidth": 1,
      // "geofencePeriod": 10,
      "networkSelectorType": "ANY",
      "networkProtocolType": "DHCP",
      "name": this.gatewayTemplateForm.value.name,
      "description": this.gatewayTemplateForm.value.description,
      "ssid": this.gatewayTemplateForm.value.ssid,
      "passphrase": this.gatewayTemplateForm.value.passphrase,
      "siteId": this.siteId,
      "adding": true
    }

    this.templateProvider.addGatewayTemplate(template).subscribe(() => {

      this.LoadingProvider.hideLoader();

      this.toastProvider.present(this.translateService.instant('BLU_TEMPLATE_SUCCESS'));


      this.modalCtrl.dismiss(true);
      // this.viewCtrl.dismiss(true);
    }, (error) => {

      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
      else {

        this.modalCtrl.dismiss(false);
      }
    });
  }

  private updateGateway() {

    this.LoadingProvider.showLoader();

    var template = {
      templateId: this.templateId,
      securityType: {
        securityTypeKey: this.gatewayTemplateForm.value.security,
        enterprise: this.gatewayTemplateForm.value.security === "Enterprise"
      },
      name: this.gatewayTemplateForm.value.name,
      description: this.gatewayTemplateForm.value.description,
      ssid: this.gatewayTemplateForm.value.ssid,
      passphrase: this.gatewayTemplateForm.value.passphrase,
      siteId: this.siteId
    }

    this.templateProvider.updateGatewayTemplate(template).subscribe(() => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      this.toastProvider.present(this.translateService.instant('BLU_TEMPLATE_UPDATED'));

      this.modalCtrl.dismiss(true);

    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
      else {

        this.modalCtrl.dismiss(false);
      }
    });
  }

  private initializeTemplateForm() {

    this.gatewayTemplateForm = this.formBuilder.group({
      name: [this.gatewayTemplateInfo.name, [Validators.required]],
      security: [this.gatewayTemplateInfo.securityType.securityTypeKey],
      description: [this.gatewayTemplateInfo.description],
      passphrase: [this.gatewayTemplateInfo.passphrase, [Validators.required]],
      ssid: [this.gatewayTemplateInfo.ssid, [Validators.required]],
    });

    //Prepare validator to show error messages
    this.genericValidator = new GenericValidator(
      this.translateService.instant('gateWayTemplate.validation'));

    this.subscribeFormValueChanges();

  }

  private subscribeFormValueChanges() {
    //subscribe to the control value changes observable and
    //perform validation.
    this.gatewayTemplateForm.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.gatewayTemplateForm);
    });
  }

  isActiveToggleTextPassword: Boolean = true;
  public toggleTextPassword(): void {
    this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword == true) ? false : true;
  }
  public getType() {
    return this.isActiveToggleTextPassword ? 'password' : 'text';
  }

  //#endregion

}
