import { Component, Input, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'beacon-list',
  templateUrl: 'beacon-list.html'
})
export class BeaconListComponent {

  @Input()
  list: Array<any>;

  @Output()
  beaconSelected: EventEmitter<any> = new EventEmitter();

  constructor() {

  }

  onBeaconClick(item: any) {
    this.beaconSelected.emit(item);
  }

}
