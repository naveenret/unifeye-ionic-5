import { Component, Input, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavController, ModalController, AlertController } from '@ionic/angular';
import { TemplateProvider } from 'src/app/providers/templates/templates';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { ProjectProvider } from 'src/app/providers/project/project';
import { BluEditTemplatePage } from 'src/app/@pages/blu-edit-template/blu-edit-template.page';

@Component({
  selector: 'template-list',
  templateUrl: 'template-list.html'
})
export class TemplateListComponent {

  @Input()
  templates: Array<any>;

  @Input()
  showEditBtn: boolean;

  @Input()
  showDeleteBtn: boolean;

  @Output()
  templateSelected: EventEmitter<any> = new EventEmitter();

  @Output()
  templateListChanged: EventEmitter<any> = new EventEmitter();
  siteId: any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public templateProvider: TemplateProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public projectProvider: ProjectProvider,
    public translateService: TranslateService,
    public alertCtrl: AlertController) {

    this.siteId = this.projectProvider.getSiteId();

  }

  onTemplateClick(template) {
    this.templateSelected.emit(template);
  }

  async editTemplateClicked(templateId) {
    let modal = await this.modalCtrl.create(
      {
        component: BluEditTemplatePage,
        componentProps: {
          templateId: templateId
        }
      });
    modal.onDidDismiss().then((data) => {
      if (data) {
        // Emit to BluSettingsPage component, so we can reload template list 
        this.templateListChanged.emit();
      }
    });
    await modal.present();
  }

  async deleteButtonClicked(templateId) {
    if (this.siteId !== 0) {
      const confirm =await this.alertCtrl.create({
        header: this.translateService.instant('DELETE_BUTTON'),
        message: this.translateService.instant('BLU_TEMPLATE_DELETE_CONFIRMATION'),
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('NO'),
            handler: () => {
              // console.log('No clicked');
            }
          },
          {
            text: this.translateService.instant('YES'),
            handler: () => {
              this.deleteTemplate(templateId);
            }
          }
        ]
      });
      await confirm.present();
    }
    else {
      const prompt = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Cannot delete gateway template from currently selected (All) location. Please change your location from dashboard.',
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('OK'),
            handler: data => {
              this.navCtrl.navigateRoot('tabs');
            }
          }
        ]
      });
      prompt.present();
    }

  }

  private deleteTemplate(templateId) {

    this.LoadingProvider.showLoader();

    this.templateProvider.deleteGatewayTemplate(templateId, this.siteId).subscribe(() => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      // Emit to BluSettingsPage component, so we can reload template list 
      this.templateListChanged.emit();

      this.toastProvider.present(this.translateService.instant('BLU_TEMPLATE_DELETED'));

    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

}
