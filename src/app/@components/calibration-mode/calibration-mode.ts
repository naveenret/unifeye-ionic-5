import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { util } from 'src/app/@utils/util';

@Component({
  selector: 'calibration-mode',
  templateUrl: 'calibration-mode.html'
})
export class CalibrationModeComponent implements  OnInit {
 

  //#region PUBLIC MEMBERS

  calibrationForm:FormGroup = null;

  @Input() devicePackat: string;
  @Input() temperatureString:string;
  prefillData: string[];
  is10K: boolean = true;

  calType: string = "";
  serialNo: string = "";
  ch1Offset_10K: string = "";
  ch1Offset25_100K: string = "";
  ch1OffsetNeg40_100K: string = "";
  ch1OffsetNeg20_100K: string = "";
  ch1Offset0_100K: string = "";
  ch1Offset60_100K: string = "";

  ch2Offset25_10K: string = "";
  ch2Offset25_100K: string = "";
  ch2OffsetNeg40_100K: string = "";
  ch2OffsetNeg20_100K: string = "";
  ch2Offset0_100K: string = "";
  ch2Offset60_100K: string = "";

  //#endregion


  //#region CONSTRUCTOR

  constructor( public formBuilder: FormBuilder) {
    // var formData = "*11&0067#*12&-065#*13&0000#*14&0001#*15&0002#*16&0000#*21&064#*22&-062#*23&0000#*24&0001#*25&0002#*26&0000#*30&CAT001#*01#$/";
  }

  //#endregion


  //#region IMPLEMENTATION METHODS

  ngOnInit(): void {

    const prefillData = this.devicePackat.split('#');
    this.preparePrefillData(prefillData);

    this.initializeCalibrationForm();
  }

  //#endregion


  //#region PUBLIC MEMBERS

  preparePrefillData(prefillForms: Array<string>) {

    if (prefillForms[0].startsWith('*11')) {
      this.calType = "*11";
    }
    else {
      this.calType = "*12";
    }

    prefillForms.forEach((prefill) => {

      if (prefill.startsWith('*11&')) {
        this.ch1Offset_10K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*12&')) {
        this.ch1Offset25_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*13&')) {
        this.ch1OffsetNeg40_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*14&')) {
        this.ch1OffsetNeg20_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*15&')) {
        this.ch1Offset0_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*16&')) {
        this.ch1Offset60_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*21&')) {
        this.ch2Offset25_10K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*22&')) {
      
        this.ch2Offset25_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*23&')) {
     
        this.ch2OffsetNeg40_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*24&')) {
        this.ch2OffsetNeg20_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*25&')) {
        this.ch2Offset0_100K = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*26&')) {
        this.ch2Offset60_100K  = this.formatTemprature(prefill);
      }
      else if (prefill.startsWith('*30&')) {
        let temperature = prefill;
        this.serialNo = temperature.substring(4, temperature.length).replace('CAT','');
      }
    });
  }

  formatTemprature(temperature) {

    var res = "";
    var includeZero = true;

    temperature = temperature.substring(4, temperature.length);
    temperature = temperature.split("").reverse().join("");

    for (var i = 0; i < temperature.length; i++) {

      if (includeZero) {
        res += temperature.charAt(i);

        if (temperature.charAt(i) !== "0") {
          includeZero = false;
        }
      }
      else {
        if (temperature.charAt(i) != 0) {
          res += temperature.charAt(i);
        }
      }
    }

    if (res === "0000") {
      res = "0";
    }

    return res.split("").reverse().join("");
  }

  prepareSubmitPackate() {

    const formData = [];

    formData.push("*11&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1Offset_10K, 4)+ "#");

    formData.push("*12&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1Offset25_100K, 4) + "#");

    formData.push("*13&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1OffsetNeg40_100K, 4) + "#");

    formData.push("*14&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1OffsetNeg20_100K, 4) + "#");

    formData.push("*15&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1Offset0_100K, 4) + "#");

    formData.push("*16&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch1Offset60_100K, 4) + "#");

    formData.push("*21&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2Offset25_10K, 4) + "#");

    formData.push("*22&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2Offset25_100K, 4) + "#");

    formData.push("*23&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2OffsetNeg40_100K, 4) + "#");

    formData.push("*24&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2OffsetNeg20_100K, 4) + "#");

    formData.push("*25&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2Offset0_100K, 4) + "#");

    formData.push("*26&");
    formData.push(this.padStringLeft(this.calibrationForm.value.ch2Offset60_100K, 4) + "#");

    formData.push("*30&");
    formData.push("CAT" + this.serialNo + "#");

    formData.push("*01#");
    formData.push('$');

    let formDataStr = formData.toString().replace(/,/g, '');
    var checksumInfo = util.checksumGenerator(formDataStr).toString();
    formDataStr = formDataStr.concat(checksumInfo);

    const size = 126 - formDataStr.length;

    for (var index = 0; index < size; index++) {
      formDataStr = formDataStr.concat('0');
    }

    return formDataStr;
  }

  padStringLeft(str, len) {

    while (str.length < len) {
      if (str.startsWith('-')) {
        str = str.replace('-', '');
        str = '-' + '0' + str;
      }
      else {
        str = '0' + str;
      }
    }

    return str;
  }

  is10CalType() {
    return this.calType === "*11";
  }

  is100CalType() {
    return this.calType !== "*11";
  }

  //#endregion


  //#region PRIVATE METHODS

  private initializeCalibrationForm() {

    this.calibrationForm = this.formBuilder.group({
      ch1Offset_10K: [this.ch1Offset_10K, Validators.required],
      ch2Offset25_10K: [this.ch2Offset25_10K, Validators.required],
      ch1Offset25_100K: [this.ch1Offset25_100K, Validators.required],
      ch1OffsetNeg40_100K: [this.ch1OffsetNeg40_100K, Validators.required],
      ch1OffsetNeg20_100K: [this.ch1OffsetNeg20_100K, Validators.required],
      ch1Offset0_100K: [this.ch1Offset0_100K, Validators.required],
      ch1Offset60_100K: [this.ch1Offset60_100K, Validators.required],
      ch2Offset25_100K: [this.ch2Offset25_100K, Validators.required],
      ch2OffsetNeg40_100K: [this.ch2OffsetNeg40_100K, Validators.required],
      ch2OffsetNeg20_100K: [this.ch2OffsetNeg20_100K, Validators.required],
      ch2Offset0_100K: [this.ch2Offset0_100K, Validators.required],
      ch2Offset60_100K: [this.ch2Offset60_100K, Validators.required]
    });
  }

  //#endregion

}
