import { Component } from '@angular/core';
import {DeviceType} from '../../@constants/app.constant'
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'device-type-selection-modal',
  templateUrl: 'device-type-selection-modal.html'
})
export class DeviceTypeSelectionModalComponent {

  //#region PUBLIC MEMBERS

  DeviceType = DeviceType;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public modalCtrl: ModalController) {

  }

  //#endregion

  //#region PUBLIC MEMBERS

  //Represents the click handler for the close button
  dismiss() {

    //close the model.
    this.modalCtrl.dismiss();
  }

  //Represents the click handler for the device type item selection
  onDeviceSelected(type) {

    let data = { 'type': type };
    this.modalCtrl.dismiss(data);
  }

  //#endregion

}
