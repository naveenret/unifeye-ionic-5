import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./@pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./@pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'alert-list',
    loadChildren: () => import('./@pages/alert-list/alert-list.module').then( m => m.AlertListPageModule)
  },
  {
    path: 'beacon-list',
    loadChildren: () => import('./@pages/beacon-list/beacon-list.module').then( m => m.BeaconListPageModule)
  },
  {
    path: 'blu-bulk-upload-beacons-page',
    loadChildren: () => import('./@pages/blu-bulk-upload-beacons-page/blu-bulk-upload-beacons-page.module').then( m => m.BluBulkUploadBeaconsPagePageModule)
  },
  {
    path: 'blufilist',
    loadChildren: () => import('./@pages/blufilist/blufilist.module').then( m => m.BlufilistPageModule)
  },
  {
    path: 'beacon-detail',
    loadChildren: () => import('./@pages/beacon-detail/beacon-detail.module').then( m => m.BeaconDetailPageModule)
  },
  {
    path: 'blufi-detail',
    loadChildren: () => import('./@pages/blufi-detail/blufi-detail.module').then( m => m.BlufiDetailPageModule)
  },
  {
    path: 'item-detail',
    loadChildren: () => import('./@pages/item-detail/item-detail.module').then( m => m.ItemDetailPageModule)
  },
  {
    path: 'blu-add-template',
    loadChildren: () => import('./@pages/blu-add-template/blu-add-template.module').then( m => m.BluAddTemplatePageModule)
  },
  {
    path: 'blu-edit-template',
    loadChildren: () => import('./@pages/blu-edit-template/blu-edit-template.module').then( m => m.BluEditTemplatePageModule)
  },
  {
    path: 'add-page',
    loadChildren: () => import('./@pages/add-page/add-page.module').then( m => m.AddPagePageModule)
  },
  {
    path: 'blu-beacon-provision',
    loadChildren: () => import('./@pages/blu-beacon-provision/blu-beacon-provision.module').then( m => m.BluBeaconProvisionPageModule)
  },
  {
    path: 'blu-blufi-provision',
    loadChildren: () => import('./@pages/blu-blufi-provision/blu-blufi-provision.module').then( m => m.BluBlufiProvisionPageModule)
  },
  {
    path: 'blufi-template-section',
    loadChildren: () => import('./@pages/blufi-template-section/blufi-template-section.module').then( m => m.BlufiTemplateSectionPageModule)
  },
  {
    path: 'beacon-template-section',
    loadChildren: () => import('./@pages/beacon-template-section/beacon-template-section.module').then( m => m.BeaconTemplateSectionPageModule)
  },
  {
    path: 'beacon-input',
    loadChildren: () => import('./@pages/beacon-input/beacon-input.module').then( m => m.BeaconInputPageModule)
  },
  {
    path: 'blufi-input',
    loadChildren: () => import('./@pages/blufi-input/blufi-input.module').then( m => m.BlufiInputPageModule)
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
