import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluEditTemplatePage } from './blu-edit-template.page';

describe('BluEditTemplatePage', () => {
  let component: BluEditTemplatePage;
  let fixture: ComponentFixture<BluEditTemplatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluEditTemplatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluEditTemplatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
