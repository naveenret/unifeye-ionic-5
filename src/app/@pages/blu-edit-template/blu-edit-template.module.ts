import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluEditTemplatePageRoutingModule } from './blu-edit-template-routing.module';

import { BluEditTemplatePage } from './blu-edit-template.page';
import { ComponentsModule } from 'src/app/@components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluEditTemplatePageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule.forChild()
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [BluEditTemplatePage]
})
export class BluEditTemplatePageModule {}
