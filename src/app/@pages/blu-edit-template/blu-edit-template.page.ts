import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blu-edit-template',
  templateUrl: './blu-edit-template.page.html',
  styleUrls: ['./blu-edit-template.page.scss'],
})
export class BluEditTemplatePage implements OnInit {

  @Input() templateId: any = null;
  //#region CONSTRUCTOR

  constructor(
    public viewCtrl: ModalController,
    public activatedRoute: ActivatedRoute) {
  }

  //#endregion

  //#region PUBLIC METHODS

  public close() {
    this.viewCtrl.dismiss(false);
  }

  //#endregion
  ngOnInit() {
  }

}
