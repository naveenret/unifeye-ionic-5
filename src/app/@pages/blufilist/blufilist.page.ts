import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { BlufiProvider } from 'src/app/providers/blufi/blufi';

@Component({
  selector: 'app-blufilist',
  templateUrl: './blufilist.page.html',
  styleUrls: ['./blufilist.page.scss'],
})
export class BlufilistPage implements OnInit {



  isSearchBarOpened =false;
  //#region PUBLIC MEMBERS

  //Represents the blufi list model.
  blufis: Array<any>;
  filteredBlufi: Array<any>;
  //Represents the site id for which the beacon needs to be loaded
  siteId: Number;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public blufiProvider: BlufiProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    debugger;
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        this.siteId = res && res!.siteId || '';
      })
      resolve();
    }).then(() => {
      //load the blufis for a give site.
      if (this.siteId === 0) {
        this.loadAllBlufiList();
      } else {
        this.loadBlufiList();
      }
    })
  }

  //#endregion

  //#region PRIVATE METHODS
  private initializeItems(): void {
    this.filteredBlufi = this.blufis;
  }

  getItems(evt) {
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.filteredBlufi = this.filteredBlufi.filter(currentGoal => {
      let result = false;
      if (currentGoal.name) {
        if (currentGoal.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          result = true;
        }
      }
      if (currentGoal.sid64) {
        if (currentGoal.sid64.indexOf(searchTerm) > -1) {
          result = true;
        }
      }
      if (currentGoal.macAddress) {
        if (currentGoal.macAddress.indexOf(searchTerm.toLowerCase()) > -1) {
          result = true;
        }
      }
      return result;
    });
  }
  //load the blufis for a site.
  private loadBlufiList() {

    //show loader.
    this.LoadingProvider.showLoader();

    //load blufis for a site
    this.blufiProvider.loadBlufiList(this.siteId).subscribe((blufis: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the blufis model.
      this.blufis = blufis;
      this.filteredBlufi = blufis;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  private loadAllBlufiList() {

    //show loader.
    this.LoadingProvider.showLoader();

    //load blufis for a site
    this.blufiProvider.loadAllBlufiList(this.siteId).subscribe((blufis: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the blufis model.
      this.blufis = blufis;
      this.filteredBlufi = blufis;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  public async deleteBlufi(bID) {
    if (this.siteId !== 0) {
      this.deleteBlufis(bID);
    }
    else {
      const prompt = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Cannot delete blufi from currently selected (All) location. Please change your location from dashboard.',
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('OK'),
            handler: data => {
              this.navCtrl.navigateRoot('tabs');
            }
          }
        ]
      });
      prompt.present();
    }
  }

  private deleteBlufis(bID) {

    this.LoadingProvider.showLoader();

    this.blufiProvider.deleteBlufis(this.siteId, [bID]).subscribe(() => {

      this.blufis = this.blufis.filter((x) => x.deviceId !== bID);
      this.filteredBlufi = this.blufis;
      //hide the loader.
      this.LoadingProvider.hideLoader();

      this.toastProvider.present('Successfully Deleted');

    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  openDetails(gateway: any) {
    this.navCtrl.navigateForward('blufi-detail', { queryParams: { selectedGateway: gateway, siteId: this.siteId } });
  }

  ionViewWillEnter() {


  }
  //#endregion
}
