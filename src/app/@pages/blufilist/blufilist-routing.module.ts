import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlufilistPage } from './blufilist.page';

const routes: Routes = [
  {
    path: '',
    component: BlufilistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlufilistPageRoutingModule {}
