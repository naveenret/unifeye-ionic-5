import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlufilistPageRoutingModule } from './blufilist-routing.module';

import { BlufilistPage } from './blufilist.page';
import { ComponentsModule } from 'src/app/@components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlufilistPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
  declarations: [BlufilistPage]
})
export class BlufilistPageModule {}
