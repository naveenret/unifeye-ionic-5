import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlufilistPage } from './blufilist.page';

describe('BlufilistPage', () => {
  let component: BlufilistPage;
  let fixture: ComponentFixture<BlufilistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlufilistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlufilistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
