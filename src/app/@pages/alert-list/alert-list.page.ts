import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { AlertProvider } from 'src/app/providers/alert/alert';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.page.html',
  styleUrls: ['./alert-list.page.scss'],
})
export class AlertListPage implements OnInit {


  //#region PUBLIC MEMBERS

  //Represents the alert list model.
  alerts: Array<any>;

  //Represents the site id for which the alerts needs to be loaded
  siteId: Number;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public alertProvider: AlertProvider,
    public activatedRoute: ActivatedRoute) {

    //get the site id from the params.


  }

  //#endregion


  ngOnInit() {
    debugger;
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        this.siteId = res && res!.siteId || '';
      })
      resolve();
    }).then(() => {
      //load the alerts for a give site.
      if (this.siteId === 0) {
        this.loadAllAlertList();
      } else {
        this.loadAlertList();
      }
    })
    console.log('this.siteId', this.siteId);
  }


  //#region PRIVATE METHODS

  //load the alerts for a give site.
  private loadAlertList() {

    //show loader.
    this.LoadingProvider.showLoader();

    this.alertProvider.loadAlertList(this.siteId).subscribe((alerts: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the alerts model.
      this.alerts = alerts;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  private loadAllAlertList() {

    //show loader.
    this.LoadingProvider.showLoader();

    this.alertProvider.loadAllAlertList(this.siteId).subscribe((alerts: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the alerts model.
      this.alerts = alerts;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  //#endregion

}
