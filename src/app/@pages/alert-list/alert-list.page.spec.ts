import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlertListPage } from './alert-list.page';

describe('AlertListPage', () => {
  let component: AlertListPage;
  let fixture: ComponentFixture<AlertListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlertListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
