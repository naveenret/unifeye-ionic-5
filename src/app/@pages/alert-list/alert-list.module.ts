import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlertListPageRoutingModule } from './alert-list-routing.module';

import { AlertListPage } from './alert-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlertListPageRoutingModule,    
    TranslateModule.forChild(),
    ReactiveFormsModule,
    ComponentsModule,
    IonicSelectableModule
  ],
  declarations: [AlertListPage]
})
export class AlertListPageModule {}
