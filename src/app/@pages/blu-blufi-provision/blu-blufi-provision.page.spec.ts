import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluBlufiProvisionPage } from './blu-blufi-provision.page';

describe('BluBlufiProvisionPage', () => {
  let component: BluBlufiProvisionPage;
  let fixture: ComponentFixture<BluBlufiProvisionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluBlufiProvisionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluBlufiProvisionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
