import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluBlufiProvisionPageRoutingModule } from './blu-blufi-provision-routing.module';

import { BluBlufiProvisionPage } from './blu-blufi-provision.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluBlufiProvisionPageRoutingModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    ComponentsModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [BluBlufiProvisionPage]
})
export class BluBlufiProvisionPageModule {}
