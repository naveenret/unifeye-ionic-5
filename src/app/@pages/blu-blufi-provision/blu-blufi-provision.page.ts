import { Component, OnInit } from '@angular/core';
import { Blufi } from 'src/app/models/Blufi';
import { NavController } from '@ionic/angular';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';
import { DeviceType } from 'src/app/@constants/app.constant';

@Component({
  selector: 'app-blu-blufi-provision',
  templateUrl: './blu-blufi-provision.page.html',
  styleUrls: ['./blu-blufi-provision.page.scss'],
})
export class BluBlufiProvisionPage implements OnInit {


  //#region PUBLIC MEMBER

  //Blufi list which serves as a datasource.
  list: Array<Blufi> = [];

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public provisioningProvider: ProvisioningProvider) {


    this.provisioningProvider.setNav(navCtrl);

    this.provisioningProvider.startProvisioning(DeviceType.Blufi,
      this.handleBlufiFoundEvent.bind(this),
      this.handleBlufiLostEvent.bind(this));
  }

  ionViewWillEnter() { console.log("ionViewWillEnter"); }
  ionViewDidEnter() { console.log("ionViewDidEnter"); }
  ionViewWillLeave() { console.log("ionViewWillLeave"); }
  ionViewDidLeave() {
    this.provisioningProvider.stopScan();
    console.log("ionViewDidLeave");
  }
  //#endregion

  //#region PUBLIC METHODS

  //Represents the blufi selection handler.
  onBlufiSelected(blufi: Blufi) {

    //stop the blufi scanning before switching to the template selection page.
    // this.stopScan();

    //Go to the template selection page.
    this.navCtrl.navigateForward('blufi-template-section', { queryParams: { blufi: blufi } })
  }

  //#endregion

  //#region PRIVATE METHODS

  //blufi found handler/
  private handleBlufiFoundEvent(blufi: Blufi) {

    //check if the blufi is already in the local list.
    var blf = this.list.filter(x => x.id.toString() === blufi.id.toString());

    //if blufi not added in the local list.
    if (blf && blf.length === 0) {

      //add the blufi to the list so that it will be displayed on the ui.
      this.list.push(blufi);
    }
    else {
      //Do nothing..
    }
  }

  //blufi lost handler.
  private handleBlufiLostEvent(id: string) {

    //remove the lost blufi from the UI.
    this.list = this.list.filter(x => x.id.toString() === id);
  }

  //#endregion


  ngOnInit() {
  }

}
