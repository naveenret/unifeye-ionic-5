import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BluBlufiProvisionPage } from './blu-blufi-provision.page';

const routes: Routes = [
  {
    path: '',
    component: BluBlufiProvisionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BluBlufiProvisionPageRoutingModule {}
