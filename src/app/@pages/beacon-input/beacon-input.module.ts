import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeaconInputPageRoutingModule } from './beacon-input-routing.module';

import { BeaconInputPage } from './beacon-input.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeaconInputPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule.forChild()
  ],
  declarations: [BeaconInputPage]
})
export class BeaconInputPageModule {}
