import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeaconInputPage } from './beacon-input.page';

describe('BeaconInputPage', () => {
  let component: BeaconInputPage;
  let fixture: ComponentFixture<BeaconInputPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeaconInputPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeaconInputPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
