import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ProvisionInfo } from 'src/app/models/ProvisionInfo';
import { GenericValidator } from 'src/app/validators/generic-validator';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-beacon-input',
  templateUrl: './beacon-input.page.html',
  styleUrls: ['./beacon-input.page.scss'],
})
export class BeaconInputPage implements OnInit {


  //#region PUBLIC MEMBER

  //Represents the beacon input form bind to the UI..
  beaconInputForm: FormGroup;

  //Represents the provision model passed from the template selection form.
  provisionInfo: ProvisionInfo;

  //Represents the validation error messages for controls
  displayMessage: { [key: string]: string } = {};

  //#endregion

  //Performs the validation for the form.
  private genericValidator: GenericValidator;

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public provisioningProvider: ProvisioningProvider,
    public translateService: TranslateService,
    private formBuilder: FormBuilder) {

    new Promise((resolve) => {
      this.activatedRoute.queryParams.subscribe((res: any) => {
        if (res) {
          this.provisionInfo = res;
        }
      })
      resolve();
    })
      .then(() => {

        //set the provision info passed from the beacon template selection page.

        //initialize the provision form.
        this.initializeForm();
      })
  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the form submit handler.
  onProvisionClick() {

    //if form is valid.
    if (this.beaconInputForm.valid) {

      //start the provisioning process.
      this.startProvisioning();
    }
    else {
      // Do nothing..
    }
  }

  //#endregion

  //#region PRIVATE METHODS

  //Initialize the form.
  private initializeForm() {

    //initialize the form.
    this.beaconInputForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      note: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });

    //Prepare validator to show error messages
    this.genericValidator = new GenericValidator(
      this.translateService.instant('beaconInput.validation'));

    this.subscribeFormValueChanges();
  }

  private subscribeFormValueChanges() {
    //subscribe to the control value changes observable and
    //perform validation.
    this.beaconInputForm.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.beaconInputForm);
    });
  }

  //start provisioning
  private startProvisioning() {

    //prepare the provision model.
    let param = {
      "id": this.provisionInfo.beacon.id,
      "templateId": this.provisionInfo.template.id,
      "name": this.beaconInputForm.value.name,
      "notes": this.beaconInputForm.value.note
    };

    //provision device.
    this.provisioningProvider.provisionDevice(param);
  }

  //#endregion

  //#region IMPLEMENTATION METHOD

  //#endregion

  ngOnInit() {
  }

}
