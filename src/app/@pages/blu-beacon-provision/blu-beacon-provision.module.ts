import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluBeaconProvisionPageRoutingModule } from './blu-beacon-provision-routing.module';

import { BluBeaconProvisionPage } from './blu-beacon-provision.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluBeaconProvisionPageRoutingModule,
    TranslateModule.forChild(),
    ComponentsModule,
    ReactiveFormsModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [BluBeaconProvisionPage]
})
export class BluBeaconProvisionPageModule {}
