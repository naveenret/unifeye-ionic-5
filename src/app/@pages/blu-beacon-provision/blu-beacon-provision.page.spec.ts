import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluBeaconProvisionPage } from './blu-beacon-provision.page';

describe('BluBeaconProvisionPage', () => {
  let component: BluBeaconProvisionPage;
  let fixture: ComponentFixture<BluBeaconProvisionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluBeaconProvisionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluBeaconProvisionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
