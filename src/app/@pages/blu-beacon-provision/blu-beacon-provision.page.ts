import { Component, OnInit } from '@angular/core';
import { Beacon } from 'src/app/models/Beacon';
import { NavController } from '@ionic/angular';
import { DeviceType } from 'src/app/@constants/app.constant';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';

@Component({
  selector: 'app-blu-beacon-provision',
  templateUrl: './blu-beacon-provision.page.html',
  styleUrls: ['./blu-beacon-provision.page.scss'],
})
export class BluBeaconProvisionPage implements OnInit {


  //#region PUBLIC MEMBER

  //Beacon list which serves as a datasource.
  list: Array<Beacon> = [];

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public provisioningProvider: ProvisioningProvider) {

    this.provisioningProvider.setNav(navCtrl);

    this.provisioningProvider.startProvisioning(DeviceType.Beacon,
      this.handleBeaconFoundEvent.bind(this),
      this.handleBeaconLostEvent.bind(this));
  }

  ionViewWillEnter() { console.log("ionViewWillEnter");	}
  ionViewDidEnter() {console.log("ionViewDidEnter");}
  ionViewWillLeave() {console.log("ionViewWillLeave");}
  ionViewDidLeave() {
    this.provisioningProvider.stopScan();
    console.log("ionViewDidLeave");
  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the beacon selection handler.
  public onBeaconSelected(beacon: Beacon) {

    //Go to the template selection page.
    this.navCtrl.navigateForward('beacon-template-section', { queryParams:  {beacon: beacon} })
  }

  //#endregion

  //#region PRIVATE METHODS

  //handle the beacon found event.
  private handleBeaconFoundEvent(beacon:Beacon) {

    //check if the beacon is already in the local list.
    var bcn = this.list.filter(x => x.id.toString() === beacon.id.toString());

    //if beacon not added in the local list.
    if (bcn && bcn.length === 0) {

      //add the beacon to the list so that it will be displayed on the ui.
      this.list.push(beacon);
    }
    else {
      //Do nothing..
    }
  }

  //handle the beacon lost event
  private handleBeaconLostEvent(id: string) {

    this.list = this.list.filter(x => x.id.toString() === id);
  }

  //#endregion
  ngOnInit() {
  }

}
