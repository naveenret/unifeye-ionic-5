import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { TemplateProvider } from 'src/app/providers/templates/templates';
import { ProjectProvider } from 'src/app/providers/project/project';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { Common } from 'src/app/providers/common/common';
import { BluAddTemplatePage } from '../blu-add-template/blu-add-template.page';

@Component({
  selector: 'app-blu-settings',
  templateUrl: './blu-settings.page.html',
  styleUrls: ['./blu-settings.page.scss'],
})
export class BluSettingsPage implements OnInit {

  ngOnInit() {
  }

  //#region PUBLIC MEMBERS

  listtype = 'gateways';

  blufiTemplates: Array<any>;

  beaconTemplates: Array<any>;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public activatedRoute: ActivatedRoute,
    public LoadingProvider: LoadingProvider,
    public templateProvider: TemplateProvider,
    public projectProvider: ProjectProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public modalCtrl: ModalController,
    public common: Common) {

  }

  //#endregion

  //#region PUBLIC MEMBERS

  public ionViewDidLoad() {
    console.log('ionViewDidLoad BluSettingsPage');
  }

  async showLocationWarningAlert() {
    const prompt =await this.alertCtrl.create({
      header: 'Warning',
      subHeader: 'Cannot load templates for currently selected (All) location. Please change your location from dashboard.',
      buttons: [
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            this.navCtrl.navigateRoot('tabs');
          }
        }
      ]
    });
    await prompt.present();
  }

  public async addTemplate() {
    let modal =await this.modalCtrl.create({
      component: BluAddTemplatePage
    });
    modal.onDidDismiss().then((data) => {
      if (data) {
        this.getTemplates();
      }
    })
    await modal.present();
  }

  public ionViewWillEnter() {

    //Get the currently mapped project list.
    let isAllLocationSelected = this.common.getAllLocationSelected();
    if(isAllLocationSelected === true){
      this.showLocationWarningAlert()
    } else {
      this.getTemplates();
    }
  }

  //#endregion

  //#region METHODS

  getTemplates() {

    //show loader.
    this.LoadingProvider.showLoader();

    this.templateProvider.getTemplates(this.projectProvider.getSiteId())
      .subscribe((data: Array<any>) => {
        this.blufiTemplates = [];
        this.beaconTemplates = [];

        data.forEach((element, indx) => {
          if (element.iBeacon === true) {

            this.blufiTemplates.push({
              templateId: element.templateId,
              name: element.name,
              description: element.description,
              dateCreated: element.dateCreated
            });
          }
          else {
            if (element.name === "Default Beacon Template") {
              element.name = element.name.replace(/Beacon/gi, 'Transmitter');
              this.beaconTemplates.push({
                templateId: element.templateId,
                name: element.name,
                description: element.description,
                dateCreated: element.dateCreated
              });
            }
            else {
              //Do Nothing.....
            }
          }
        });

        this.beaconTemplates = this.beaconTemplates.sort((n1,n2) => n1.dateCreated - n2.dateCreated);

        this.beaconTemplates = this.beaconTemplates.slice(0, 10);

        this.LoadingProvider.hideLoader();
      }, (error) => {
        this.LoadingProvider.hideLoader();
        if (error.status !== 401) {
          this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
        }
      });
  }
  //#endregion

}
