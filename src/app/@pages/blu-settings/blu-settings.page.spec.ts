import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluSettingsPage } from './blu-settings.page';

describe('BluSettingsPage', () => {
  let component: BluSettingsPage;
  let fixture: ComponentFixture<BluSettingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluSettingsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluSettingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
