import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluSettingsPageRoutingModule } from './blu-settings-routing.module';

import { BluSettingsPage } from './blu-settings.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';
import { BluAddTemplatePageModule } from '../blu-add-template/blu-add-template.module';
import { BluEditTemplatePageModule } from '../blu-edit-template/blu-edit-template.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluSettingsPageRoutingModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    ComponentsModule,
    BluEditTemplatePageModule,
    BluAddTemplatePageModule
  ],
	schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [BluSettingsPage]
})
export class BluSettingsPageModule {}
