import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BluSettingsPage } from './blu-settings.page';

const routes: Routes = [
  {
    path: '',
    component: BluSettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BluSettingsPageRoutingModule {}
