import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Common } from 'src/app/providers/common/common';
import { DeviceType } from 'src/app/@constants/app.constant';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blu-provision',
  templateUrl: './blu-provision.page.html',
  styleUrls: ['./blu-provision.page.scss'],
})
export class BluProvisionPage implements OnInit {


  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public translateService: TranslateService,
    // public navParams: NavParams,
    public activatedRoute: ActivatedRoute,
    public common: Common) {

  }

  ionViewWillEnter() {
    let isFormProvision = false;
    new Promise((resolve) => {
      this.activatedRoute.queryParams.subscribe(res => {
        if (res && res.isFromProvision) {
          isFormProvision = res.isFormProvision;
        }
      })
      resolve();
    }).then(() => {

      let isAllLocationSelected = this.common.getAllLocationSelected();
      if (isAllLocationSelected === true) {
        this.showLocationWarningAlert()
      } else if (!isFormProvision) {
        this.showConfirm();
      }
      else {
        // this.navParams.data.isFromProvision = false;
      }
    })
  }

  async showLocationWarningAlert() {
    const prompt = await this.alertCtrl.create({
      header: 'Warning',
      subHeader: 'Cannot provision device to currently selected (All) location. Please change your location from dashboard.',
      backdropDismiss: false,
      buttons: [
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            this.navCtrl.navigateRoot('tabs');
          }
        }
      ]
    });
    await prompt.present();
  }

  async showConfirm() {

    const prompt = await this.alertCtrl.create({
      header: this.translateService.instant('BLU_PROVISION_NEW'),
      inputs: [
        {
          type: 'radio',
          label: this.translateService.instant('BLU_SENSOR'),
          value: DeviceType.Beacon,
          checked: true
        },
        {
          type: 'radio',
          label: this.translateService.instant('BLU_GATEWAY'),
          value: DeviceType.Blufi
        }
      ],
      buttons: [
        {
          text: this.translateService.instant('CANCEL'), handler: data => {
            this.navCtrl.navigateRoot('tabs');
          }
        },
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            if (data === DeviceType.Beacon) {
              // this.navCtrl.navigateForward('tabs'); // ? todo remove only for testing 
              this.navCtrl.navigateForward('blu-beacon-provision');
            }
            else {
              this.navCtrl.navigateForward('blu-blufi-provision');
            }
          }
        }
      ]
    });
    await prompt.present();
  }

  ngOnInit() {
  }

}
