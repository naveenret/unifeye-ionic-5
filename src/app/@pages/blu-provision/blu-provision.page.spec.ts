import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluProvisionPage } from './blu-provision.page';

describe('BluProvisionPage', () => {
  let component: BluProvisionPage;
  let fixture: ComponentFixture<BluProvisionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluProvisionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluProvisionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
