import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluProvisionPageRoutingModule } from './blu-provision-routing.module';

import { BluProvisionPage } from './blu-provision.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluProvisionPageRoutingModule,
    TranslateModule.forChild(),
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [BluProvisionPage]
})
export class BluProvisionPageModule {}
