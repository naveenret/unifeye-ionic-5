import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BluProvisionPage } from './blu-provision.page';

const routes: Routes = [
  {
    path: '',
    component: BluProvisionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BluProvisionPageRoutingModule {}
