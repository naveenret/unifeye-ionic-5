import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlufiDetailPage } from './blufi-detail.page';

describe('BlufiDetailPage', () => {
  let component: BlufiDetailPage;
  let fixture: ComponentFixture<BlufiDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlufiDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlufiDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
