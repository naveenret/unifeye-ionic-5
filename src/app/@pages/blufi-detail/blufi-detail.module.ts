import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlufiDetailPageRoutingModule } from './blufi-detail-routing.module';

import { BlufiDetailPage } from './blufi-detail.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlufiDetailPageRoutingModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [BlufiDetailPage]
})
export class BlufiDetailPageModule {}
