import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { BlufiProvider } from 'src/app/providers/blufi/blufi';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-blufi-detail',
  templateUrl: './blufi-detail.page.html',
  styleUrls: ['./blufi-detail.page.scss'],
})
export class BlufiDetailPage implements OnInit {



  //#region PUBLIC MEMBERS
  public blufi: any;
  public siteId: Number;
  public deviceId: any;
  public update: boolean;
  //#endregion

  //#region CONSTRUCTOR

  editForm: FormGroup;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public blufiProvider: BlufiProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public activatedRoute: ActivatedRoute,
    public fb: FormBuilder) {
    this.editForm = this.fb.group({
      csNetID: null,
      deviceId: null,
      firmwareVersion: null,
      lastCommunicationDate: null,
      lastCommunicationDateUTC: null,
      macAddress: null,
      name: null,
      network: null,
      sid64: null,
      status: null,
      type: null,
    })
  }

  ngOnInit() {
    debugger;
    this.update = false;
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        this.siteId = res && res!.siteId || '';
        this.blufi = res && res!.selectedGateway || '';
        this.editForm.patchValue({
          csNetID: this.blufi.csNetID || null,
          deviceId: this.blufi.deviceId || null,
          firmwareVersion: this.blufi.firmwareVersion || null,
          lastCommunicationDate: this.blufi.lastCommunicationDate || null,
          lastCommunicationDateUTC: this.blufi.lastCommunicationDateUTC || null,
          macAddress: this.blufi.macAddress || null,
          name: this.blufi.name || null,
          network: this.blufi.network || null,
          sid64: this.blufi.sid64 || null,
          status: this.blufi.status || null,
          type: this.blufi.type || null,
        })
        this.deviceId = this.blufi.deviceId;
      })
      resolve();
    }).then(() => {
      //load the alerts for a give site.

    })
  }


  setUpdate($event) {

    if ($event._value !== '') {
      this.update = true;
    }
  }

  async submitblufiDetailForm() {
    if (this.siteId !== 0) {
      if (this.blufi !== '') {
        this.LoadingProvider.showLoader();

        console.log('berore submit blufi',this.blufi)
        console.log('berore submit blufi',this.editForm.value)
        this.blufiProvider.updtaeBlufi(this.siteId, { "deviceId": this.deviceId, "name": this.editForm.value}).subscribe(() => {

          this.update = false;

          //hide the loader.
          this.LoadingProvider.hideLoader();

          this.toastProvider.present('Successfully Updated');

          this.navCtrl.pop();

        }, (error) => {

          //hide the loader.
          this.LoadingProvider.hideLoader();

          //display error message.
          this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));

        });
      }
      else {
        this.toastProvider.present('Please enter Gateway name.');
      }
    }
    else {
      const prompt = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Cannot update Gateway from currently selected (All) location. Please change your location from dashboard.',
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('OK'),
            handler: data => {
              this.navCtrl.navigateRoot('tabs');
            }
          }
        ]
      });
      prompt.present();
    }

  }
}
