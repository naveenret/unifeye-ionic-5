import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlufiDetailPage } from './blufi-detail.page';

const routes: Routes = [
  {
    path: '',
    component: BlufiDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlufiDetailPageRoutingModule {}
