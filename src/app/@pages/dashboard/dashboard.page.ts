import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { NavController, AlertController } from '@ionic/angular';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { Common } from 'src/app/providers/common/common';
import { ProjectProvider } from 'src/app/providers/project/project';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/providers/user/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {


  //#region PUBLIC MEMBERS
  project: Project;
  //Represents the list of project which serves as data source for the select-box.
  projects: Array<Project>;

  //Represents the currently selected project.
  selectedSiteId: any;

  //Represents the project overview info.
  projectOverview: any;

  lastSelectedSite: any;
  allLocation: Project;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public LoadingProvider: LoadingProvider,
    public common: Common,
    public alertCtrl: AlertController,
    public projectProvider: ProjectProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public user: User) {
  }

  //#endregion

  
  ngOnInit() {
    
    console.log("ng on init dashboard called");

    this.allLocation = new Project();
    this.allLocation.projectId = -1;
    this.allLocation.name = "All";
    this.allLocation.siteId = 0;
    this.LoadingProvider.showLoaderWithoutTimer();
    this.user.getSavedLocationInfo().subscribe((data: any) => {
      new Promise((resolve) => {
        this.LoadingProvider.hideLoader();
        this.lastSelectedSite = data;
        resolve(this.lastSelectedSite);
      }).then(() => {
        this.loadProjects();
      })

    }, (err) => {
      new Promise((resolve) => {
        this.LoadingProvider.hideLoader();
        this.lastSelectedSite = "{ 'lastSelectedRTSSite':0 }";
        resolve();
      }).then(() => {
        this.loadProjects();
      })

    }
    );
  }


  //#region PUBLIC METHODS

  //Get the currently mapped project list.
  loadProjects() {
    //show loader.
    
    this.LoadingProvider.showLoaderWithoutTimer();
    this.projectProvider.loadProjects().subscribe((data: any) => {
      //if data found.
      if (data && data.length > 0) {
        //set the projects data.
        this.projects = data.filter(x => x.projectID !== 0)
          .map(x => ({ projectId: x.projectID, name: x.siteName, siteId: x.siteID }));
        this.projects.splice(0, 0, this.allLocation);
        if (this.projects && this.projects.length > 0) {
          if (this.projectProvider.getSiteId() !== -1) {
            this.selectedSiteId = this.projectProvider.getSiteId();
            var site = this.projects.find((x: any) => x.siteId.toString() === this.selectedSiteId.toString());
            this.project = site;
          } else {
            //set selected project.
            var siteID = 0;
            if (this.lastSelectedSite && this.lastSelectedSite.lastSelectedRTSSite) {
              siteID = this.lastSelectedSite.lastSelectedRTSSite;
            }

            if (siteID && siteID !== 0) {
              var site = this.projects.find((x: any) => x.siteId.toString() === siteID.toString());

              //If last selected site is available..
              if (site && site !== undefined) {
                //selecte the last selected location.
                this.project = site;
                this.selectedSiteId = site.siteId;
                this.projectProvider.setSite(site);
              }
              else {
                //select the first location from the sites list
                this.project = this.projects[1];
                this.selectedSiteId = this.projects[1].siteId;
                this.projectProvider.setSite(this.projects[1]);
              }
            } else {
              this.project = this.projects[1];
              this.selectedSiteId = this.projects[1].siteId;
              this.projectProvider.setSite(this.projects[1]);
            }
          }
          if (this.projectProvider.getProjectId() !== -1) {
            this.getProjectToken(this.projectProvider.getProjectId());
          }
          //load the project details.
          if (this.selectedSiteId === 0) {
            this.loadAllDashboardOverview(this.selectedSiteId);
          } else {
            this.loadProjectOverview(this.selectedSiteId);
          }
        }
        else {
          this.LoadingProvider.hideLoader();
          this.showConfirm(this.translateService.instant('BLU_RTS_SITE_NOTFOUND'), this.translateService.instant('BLU_RTS_SITE_NOTFOUND_MESSAGE'));
        }
        this.LoadingProvider.hideLoader();
      }
      else {
        this.LoadingProvider.hideLoader();
        this.showConfirm(this.translateService.instant('BLU_SITE_NOTFOUND'), this.translateService.instant('BLU_SITE_NOTFOUND_MESSAGE'));
      }
    }, (err) => {
      this.LoadingProvider.hideLoader();
      if (err.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
   

  }

  //load the selected project overview details.
  loadProjectOverview(siteId: Number) {
    //show loader.
    this.LoadingProvider.showLoaderWithoutTimer();

    //load the project overview details.
    this.projectProvider.loadProjectOverview(siteId).subscribe((data: any) => {

      this.projectOverview = data;
      this.getProjectToken(this.projectProvider.getProjectId());
      if (this.lastSelectedSite.lastSelectedRTSSite !== siteId) {
        this.saveSelectedProject(siteId);
      }
      this.LoadingProvider.hideLoader();
    }, (err) => {
      this.LoadingProvider.hideLoader();

      //display error message.
      if (err.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  //load the selected project overview details.
  loadAllDashboardOverview(siteId: Number) {
    //show loader.
    this.LoadingProvider.showLoaderWithoutTimer();

    //load the project overview details.
    this.projectProvider.loadAllDashboardOverview(siteId).subscribe((data: any) => {

      this.projectOverview = data;
      //this.getProjectToken(this.projectProvider.getProjectId());
      this.LoadingProvider.hideLoader();
    }, (err) => {
      this.LoadingProvider.hideLoader();

      //display error message.
      if (err.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  saveSelectedProject(siteId: Number) {
    this.user.saveLastLocation(siteId).subscribe((data: any) => {
      console.log("save success " + data);
    }, (err) => {
      console.error("save failed");
    });
  }

  getProjectToken(projectId: Number) {
    //load the project overview details.
    this.projectProvider.getProjectToken(projectId).subscribe((data: any) => {
      this.common.setProjectTokenToLocalStorage(data);
    }, (err) => {
      if (err.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  //Project selection change handler.
  onProjectChange(data) {
    var site = this.projects.find((x: any) => x.siteId.toString() === data.value.siteId.toString());
    this.projectProvider.setSite(site);
    this.selectedSiteId = site.siteId;
    //load the project details.
    if (this.selectedSiteId === 0) {
      this.common.setAllLocationSelected(true);
      this.loadAllDashboardOverview(this.selectedSiteId);
    } else {
      this.common.setAllLocationSelected(false);
      this.loadProjectOverview(this.selectedSiteId);
    }
  }

  gotoBlufiListPage() {
    this.navCtrl.navigateForward('blufilist', { queryParams: { siteId: this.selectedSiteId } });
  }

  gotoBeaconListPage() {
    this.navCtrl.navigateForward('beacon-list', { queryParams: { siteId: this.selectedSiteId } });
  }

  gotoAlertListPage() {
    this.navCtrl.navigateForward('alert-list', { queryParams: { siteId: this.selectedSiteId } });
  }

  //#endregion

  //#region PRIVATE METHODS
  private async showConfirm(title, message) {

    const prompt = await this.alertCtrl.create({
      header: title,
      message: message,
      backdropDismiss: false,
      buttons: [
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            this.navCtrl.navigateRoot('login');
          }
        }
      ]
    });
    await prompt.present();
  }

  //#region IMPLEMENTATION METHOD

  ionViewWillEnter() {
    //Get the currently mapped project list.
    if (this.lastSelectedSite && this.lastSelectedSite.lastSelectedRTSSite) {

    }
    // this.loadProjects();

  }

  //#endregion
}
