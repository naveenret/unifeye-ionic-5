import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';
import { IonicSelectableModule } from 'ionic-selectable'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    ComponentsModule,
    IonicSelectableModule
    
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
