import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeaconDetailPage } from './beacon-detail.page';

describe('BeaconDetailPage', () => {
  let component: BeaconDetailPage;
  let fixture: ComponentFixture<BeaconDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeaconDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeaconDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
