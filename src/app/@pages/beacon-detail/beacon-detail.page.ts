import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { BeaconProvider } from 'src/app/providers/beacon/beacon';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-beacon-detail',
  templateUrl: './beacon-detail.page.html',
  styleUrls: ['./beacon-detail.page.scss'],
})
export class BeaconDetailPage implements OnInit {

  
  //#region PUBLIC MEMBERS
  public beacon: any;
  public siteId: Number;  
  public deviceId: any; 
  public update: boolean;
  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public beaconProvider: BeaconProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public activatedRoute: ActivatedRoute){
    }
    
    
    
    ngOnInit() {
      debugger;
      this.update = false;
      new Promise((resolve) => {
        
        this.activatedRoute.queryParams.subscribe(res => {
          this.siteId = res && res!.siteId || '';
          this.beacon = res && res!.selectedBeacon || '';
          this.deviceId = this.beacon.deviceId;
      })
      resolve();
    }).then(() => {
      //load the alerts for a give site.

    })
    console.log('this.siteId', this.siteId);
  }

  
  setUpdate($event){
    if($event._value !== ''){
      this.update = true;       
    }
  }

  async submitBeaconDetailForm(){
    if(this.siteId !== 0){
      if(this.beacon !== ''){
        this.LoadingProvider.showLoader();
        this.beaconProvider.UpdateBeacon(this.siteId,{"deviceId":this.deviceId,"name":this.beacon}).subscribe(() => {
  
          this.update = false;
  
          //hide the loader.
          this.LoadingProvider.hideLoader();
  
          this.toastProvider.present('Successfully Updated');
          
          this.navCtrl.pop();
  
        }, (error) => {
  
          //hide the loader.
          this.LoadingProvider.hideLoader();
  
          //display error message.
          this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));

        });
      }
      else{
        this.toastProvider.present('Please enter Transmitter name.');
      }
    }
    else{
      const prompt = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Cannot update Transmitter from currently selected (All) location. Please change your location from dashboard.',
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('OK'),
            handler: data => {
              this.navCtrl.navigateRoot('tabs');
            }
          }
        ]
      });
      prompt.present();
    }
    
  }
}
