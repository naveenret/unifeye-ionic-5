import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeaconDetailPageRoutingModule } from './beacon-detail-routing.module';

import { BeaconDetailPage } from './beacon-detail.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeaconDetailPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    ComponentsModule
  ],
  declarations: [BeaconDetailPage]
})
export class BeaconDetailPageModule {}
