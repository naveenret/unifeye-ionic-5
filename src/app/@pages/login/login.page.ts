import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NavController, Platform } from '@ionic/angular';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { User } from 'src/app/providers/user/user';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { AutoLoginProvider } from 'src/app/providers/autologin/autologin';
import { Common } from 'src/app/providers/common/common';
// import { CapacitorFirebaseAnalytics } from 'capacitor-firebase-analytics';
import { Plugins } from '@capacitor/core';
import { Crashlytics } from 'capacitor-crashlytics';

import { Analytics } from 'capacitor-analytics';
const { Device, AnalyticsPlugin } = Plugins;



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public signupform: FormGroup;
  public app_version: string;
  account: { username: string, password: string } = {
    username: '',
    password: ''
  };


  // Our translated text strings
  private loginErrorString: string;

  constructor(
    public translateService: TranslateService,
    public navCtrl: NavController,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public autoLoginProvider: AutoLoginProvider,
    public commonService: Common,
    public user: User,
    public platform: Platform,
    public analytics: Analytics,
    public crashlytics: Crashlytics,
    // public capacitorFirebaseAnalytics: CapacitorFirebaseAnalytics,

  ) {

    // crashlytics
    //   .logUser({
    //     name: 'naveen',
    //     email: "naveen.ret@gmail.com",
    //     id: "001"
    //   })
    //   .then(() => alert(`user logged`))
    //   .catch(err => alert(err.message));

    //
    // force a crash
    // crashlytics.crash().then(()=>{
    //   console.log('crashsed did manualy')
    // });

    // this.capacitorFirebaseAnalytics.logEvent({ name: 'Firebase_analytics_test', parameters: { 'test': 'pass' } })
    //   .then((value) => {
    //     console.log('firebase analytics passed', value);
    //   },
    //     (err) => {
    //       console.log('firebase analytics passed err', err);
    //     }).catch((err) => {
    //       console.log('firebase analytics passed err catch', err);

    //     }) 

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })


    Device.getInfo().then((value) => {
      console.log('apk verison device info ');
      console.log(value)
      this.app_version = value.appVersion || '';
    }, err => {
      console.log('apk verison device info erorrr', err)
      this.app_version = '';
    })



    let EMAILPATTERN = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';
    this.signupform = new FormGroup({
      username: new FormControl('yogi87.cse@gmail.com', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      password: new FormControl('Cooper1!', [Validators.required, Validators.minLength(6), Validators.maxLength(20)])
    });
    this.platform.ready().then(() => {
      setTimeout(() => {
        this.autoLoginProvider.attemptAutoLogin(this.doAutoLogin.bind(this));
      }, 1000)
    })

  }


  // Attempt to login in through our User service
  doLogin() {
    this.analytics.logEvent({
      name: 'NAVEEN_TEST_DO_LOGIN',
      params: { doLoginTIme: new Date() }
    });
    console.log('this.account)', this.account);
    console.log('this.account)', this);
    this.LoadingProvider.showLoader();
    if (this.signupform.get('username').status == 'VALID'
      && this.signupform.get('password').status == 'VALID') {
      var resp = this.user.login(this.account);
      let __this = this;
      resp.then(function (user) {
        console.log(user);
        if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (user.challengeName === 'PASSWORD_VERIFIER') {
          __this.toastProvider.present(this.translateService.instant('INACTIVE_USER'));
        }
        else if (user.challengeName === 'SMS_MFA' || user.challengeName === 'SOFTWARE_TOKEN_MFA') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (user.signInUserSession.accessToken.jwtToken !== '') {
          console.log(user);
          __this.LoadingProvider.hideLoader();
          __this.user._loggedIn(__this.account);
          __this.autoLoginProvider.clearCredentials();
          __this.autoLoginProvider.saveCredentials(__this.account);
          __this.commonService.setTokenToLocalStorage(user.signInUserSession.accessToken.jwtToken);
          __this.navCtrl.navigateRoot('tabs');
        }
        else {
          this.LoadingProvider.hideLoader();
          this.toastProvider.present(this.translateService.instant('INVALID_LOGIN'));
        }
      }).catch((err) => {
        console.log(err);
        __this.LoadingProvider.hideLoader();
        if (err && err.code === 'NotAuthorizedException') {
          __this.toastProvider.present(this.translateService.instant('INVALID_LOGIN'));
        }
        else if (err && err.code === 'PasswordResetRequiredException') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (err && err.code === 'UserNotConfirmedException') {
          __this.toastProvider.present(this.translateService.instant('INACTIVE_USER'));
        }
        else if (err && err.code === 'UserNotFoundException') {
          __this.toastProvider.present(this.translateService.instant('USER_Not_Found'));
        }
        else {
          __this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
        }
      });

    } else {
      this.LoadingProvider.hideLoader();
      this.toastProvider.present(this.translateService.instant('LOGIN_ERROR'));
    }
  }

  doAutoLogin(username: string, password: string) {

    
    this.analytics.logEvent({
      name: 'NAVEEN_TEST_DO_Auto',
      params: { doAutoLoginTIme: new Date() }
    });
    console.log('this.account)', this.account);
    console.log('this.account)', this);
    this.LoadingProvider.showLoader();
    if (username != ''
      && password != '') {
      this.account.username = username;
      this.account.password = password;
      var resp = this.user.login(this.account);
      let __this = this;
      resp.then(function (user) {
        if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (user.challengeName === 'PASSWORD_VERIFIER') {
          __this.toastProvider.present(this.translateService.instant('INACTIVE_USER'));
        }
        else if (user.challengeName === 'SMS_MFA' || user.challengeName === 'SOFTWARE_TOKEN_MFA') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (user.signInUserSession.accessToken.jwtToken !== '') {
          //console.log(user);
          __this.LoadingProvider.hideLoader();
          __this.user._loggedIn(__this.account);
          __this.autoLoginProvider.clearCredentials();
          __this.autoLoginProvider.saveCredentials(__this.account);
          __this.commonService.setTokenToLocalStorage(user.signInUserSession.accessToken.jwtToken);
          __this.navCtrl.navigateRoot('tabs');
        }
        else {
          this.LoadingProvider.hideLoader();
          this.toastProvider.present(this.translateService.instant('INVALID_LOGIN'));
        }
      }).catch((err) => {
        // console.error('ERROR', err);
        __this.LoadingProvider.hideLoader();
        if (err && err.code === 'NotAuthorizedException') {
          __this.toastProvider.present(this.translateService.instant('INVALID_LOGIN'));
        }
        else if (err && err.code === 'PasswordResetRequiredException') {
          __this.toastProvider.present(this.translateService.instant('ACCOUNT_LOCKED'));
        }
        else if (err && err.code === 'UserNotConfirmedException') {
          __this.toastProvider.present(this.translateService.instant('INACTIVE_USER'));
        }
        else if (err && err.code === 'UserNotFoundException') {
          __this.toastProvider.present(this.translateService.instant('USER_Not_Found'));
        }
        else {
          __this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
        }
      });
    } else {
      this.LoadingProvider.hideLoader();
      this.toastProvider.present(this.translateService.instant('LOGIN_ERROR'));
    }
  }

  guest() {
    this.navCtrl.navigateForward('add-page');
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      console.log('platform read ');
      console.log('Analytics called in ng on init Login page');
      // this.analytics.enable().then((value) => {
      //   console.log('Analytics Enabled', value);
      this.analyticsEvent();
      console.log('Analytics called in ng on init Login page');
      this.crashlyticsEvent();
      // }, (err) => {
      //   console.log('Analytics not Enabled', err);
      // }).catch((err) => {
      //   console.log('Analytics not Enabled catch', err);
      // })
    })
  }

  analyticsEvent() {
    //
    // user id
    this.analytics.setUserID({ value: '001' });

    //
    // user attributes
    // google don't allow use of sensitive data
    // like names, emails, card numbers, etc.
    this.analytics.setUserProp({
      key: 'CITY',
      value: 'TUP'
    });

    //
    // some event
    this.analytics.logEvent({
      name: 'NAVEEN_TEST_NG_ON_INIT',
      params: { items: '[1, 2, 3]', total: 254.5 }
    });
  }

  crashlyticsEvent() {
    //# NOTE: : logUser not a fun so hided now remove in future 
    // log user
    // this.crashlytics
    //   .logUser({
    //     name: "naveen",
    //     email: "test",
    //     id: "11"
    //   })
    //   .then(() => alert(`user logged`))
    //   .catch(err => alert(err.message));
  }

  async crash() {
    // note: https://github.com/stewwan/capacitor-crashlytics
    // Build phase
    // Create a Fabric account
    // Go to install instructions
    // Follow steps on Add a Run Script Build Phase
    // Follow steps on Add Your API Key
    // https://fabric.io/kits/android/crashlytics/install
    // also refer 
    // https://firebase.google.com/docs/crashlytics/get-started?platform=android
    await this.crashlytics.crash().then((value) => {
      console.log('crashsed did manualy', value);
      alert(`
      Return success but not closoing the app : 
      \n  
      \n The plugin says android comming soon ; need to  refer 
      \n https://github.com/stewwan/capacitor-crashlytics
      \n also crashlytics used  fabric account  and ne sync fabric with firebase 
        `);
    },
      err => {
        console.log('crashsed did manualy err', err);
      }).catch(err => {
        console.log('crashsed did manualy err catch', err)
      });

  }

}
