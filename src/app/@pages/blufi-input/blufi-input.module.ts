import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlufiInputPageRoutingModule } from './blufi-input-routing.module';

import { BlufiInputPage } from './blufi-input.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlufiInputPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule.forChild()
  ],
  declarations: [BlufiInputPage]
})
export class BlufiInputPageModule {}
