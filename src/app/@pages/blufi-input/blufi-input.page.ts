import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { BlufiProvisionInfo } from 'src/app/models/ProvisionInfo';
import { GenericValidator } from 'src/app/validators/generic-validator';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-blufi-input',
  templateUrl: './blufi-input.page.html',
  styleUrls: ['./blufi-input.page.scss'],
})
export class BlufiInputPage implements OnInit {


  //#region PUBLIC MEMBER

  //Represents the blufi input form bind to the UI.
  blufiInputForm: FormGroup;

  //Represents the provision model passed from the template selection form.
  provisionInfo: BlufiProvisionInfo;

  //Represents the validation error messages for controls
  displayMessage: { [key: string]: string } = {};

  //#endregion

  //Performs the validation for the form.
  private genericValidator: GenericValidator;


  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public provisioningProvider: ProvisioningProvider,
    public translateService: TranslateService,
    private formBuilder: FormBuilder) {

    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe((res: any) => {
        console.log('blufi input query params', res);
        if (res) {
          this.provisionInfo = res;
        }
      })
      resolve();
    }).then(() => {

      this.initializeForm();
    })
    //set the provision info passed from the blufi template selection page.

    //initialize the provision form.
  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the form submit handler.
  onProvisionClick() {

    //if form is valid.
    if (this.blufiInputForm.valid) {

      //start the provisioning process.
      this.startProvisioning();
    }
    else {
      // Do nothing..
    }
  }

  //#endregion

  //#region PRIVATE METHODS

  //start provisioning
  private startProvisioning() {

    //prepare the provision model.
    let param = {
      "id": this.provisionInfo.blufi.id,
      "templateId": this.provisionInfo.template.id,
      "name": this.blufiInputForm.value.name,
      "notes": this.blufiInputForm.value.note
    };

    //provision device.
    this.provisioningProvider.provisionDevice(param);
  }

  //Initialize the form.
  private initializeForm() {

    this.blufiInputForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      note: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });

    //Prepare validator to show error messages
    this.genericValidator = new GenericValidator(
      this.translateService.instant('gateWayInput.validation'));

    this.subscribeFormValueChanges();
  }

  private subscribeFormValueChanges() {
    //subscribe to the control value changes observable and
    //perform validation.
    this.blufiInputForm.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.blufiInputForm);
    });
  }

  //#endregion

  //#region IMPLEMENTATION METHOD

  //#endregion

  ngOnInit() {
  }

}
