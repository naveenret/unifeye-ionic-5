import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlufiInputPage } from './blufi-input.page';

describe('BlufiInputPage', () => {
  let component: BlufiInputPage;
  let fixture: ComponentFixture<BlufiInputPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlufiInputPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlufiInputPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
