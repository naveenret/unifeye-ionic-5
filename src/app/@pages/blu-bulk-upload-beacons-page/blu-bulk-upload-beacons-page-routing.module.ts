import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BluBulkUploadBeaconsPagePage } from './blu-bulk-upload-beacons-page.page';

const routes: Routes = [
  {
    path: '',
    component: BluBulkUploadBeaconsPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BluBulkUploadBeaconsPagePageRoutingModule {}
