import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluBulkUploadBeaconsPagePageRoutingModule } from './blu-bulk-upload-beacons-page-routing.module';

import { BluBulkUploadBeaconsPagePage } from './blu-bulk-upload-beacons-page.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluBulkUploadBeaconsPagePageRoutingModule,
    TranslateModule.forChild(),
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [BluBulkUploadBeaconsPagePage],
  providers:[BarcodeScanner]
})
export class BluBulkUploadBeaconsPagePageModule {}
