import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluBulkUploadBeaconsPagePage } from './blu-bulk-upload-beacons-page.page';

describe('BluBulkUploadBeaconsPagePage', () => {
  let component: BluBulkUploadBeaconsPagePage;
  let fixture: ComponentFixture<BluBulkUploadBeaconsPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluBulkUploadBeaconsPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluBulkUploadBeaconsPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
