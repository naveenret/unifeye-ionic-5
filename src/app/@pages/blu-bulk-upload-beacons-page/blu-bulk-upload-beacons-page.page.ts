import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { TranslateService } from '@ngx-translate/core';
import { BeaconProvider } from 'src/app/providers/beacon/beacon';
import { ActivatedRoute } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import BigNumber from "bignumber.js"

@Component({
  selector: 'app-blu-bulk-upload-beacons-page',
  templateUrl: './blu-bulk-upload-beacons-page.page.html',
  styleUrls: ['./blu-bulk-upload-beacons-page.page.scss'],
})
export class BluBulkUploadBeaconsPagePage implements OnInit {

  
  //#region PUBLIC MEMBERS

  //Represents the scanned beacon list model binded to UI.
  beacons:Array<any>  = [];

  //Represents the site id for which the beacon needs to be uploaded/provisioned
  siteId: Number;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public beaconProvider: BeaconProvider,
    public activatedRoute: ActivatedRoute) {

    //get the site id from the route params.
  }


  ngOnInit() { debugger;
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        this.siteId = res && res!.siteId || '';
      })
      resolve();
    }).then(() => {
      //load the alerts for a give site.

    })
  }
  //#endregion


  //#region CLICK HANDLERS

  //Represents the click handler for the 'SCAN QR CODE' button.
  startScanClicked() {

    //start scanning QR code.
    this.barcodeScanner.scan().then(barcodeData => {
      if(!barcodeData.cancelled){
        //try to find if the Transmitter is already scanned..
        var beacon = this.beacons.find(x => x.text === barcodeData.text);

        //If Transmitter is not scanned...
        if (beacon == undefined) {

          //convert the TrnasmitterID to SID64.
          var id64 = new BigNumber(barcodeData.text, 10).toString(16).toUpperCase();        
          //Add the scanned trnaasmitter to the list so that the UI will be updated.
          this.beacons.push({
            text: barcodeData.text,
            id64: id64
          });
        
        }
        else {

          //Show transmitter already scanned toast message.
          this.toastProvider.present(this.translateService.instant('BLU_TRANSMITTER_ALREADY_SCANNED'));
        }
      }
    }).catch(err => {

      //logs the error to console.
      console.log('Error', JSON.stringify(err));

      //Show transmitter scanning error message.
      this.toastProvider.present(this.translateService.instant('BLU_UANBLE_TO_SCAN'));
    });
  }

  //Represents the clich handler for the Delete button.
  deleteBeaconClicked(beacon) {

    //Remove the Transmitter from the list so that the UI will be updated.
    this.beacons = this.beacons.filter(x => x.text !== beacon.text);
  }

  //Upload the becons
  uploadBeaconsClicked() {

    //get the sid64 for the beacons
    var sid64 = this.beacons.map(x => x.id64);

    if (sid64 && sid64.length > 0) {

      //show the loader.
      this.LoadingProvider.showLoader(this.translateService.instant('BLU_UPLOADING_TRANSMITTERS'));

      //call the api to upload the beacons.
      this.beaconProvider.bulkUploadBeacons(this.siteId, sid64).subscribe((beacons: Array<any>) => {

        //hide the loader.
        this.LoadingProvider.hideLoader();

        //this.navCtrl.popToRoot();

        //redirect the user to the Beacon list page.
        this.navCtrl.navigateForward('beacon-list', { queryParams: { siteId: this.siteId} });
      }, (error) => {

        //hide the loader.
        this.LoadingProvider.hideLoader();

        //display error message.
        if (error.status !== 401) {

          this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
        }
      });
    }
  }

  //#endregion

}
