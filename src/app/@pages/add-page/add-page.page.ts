import { Component, OnInit, ChangeDetectionStrategy, NgZone } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { BLE } from '@ionic-native/ble/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

declare var cordova: any;

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.page.html',
  styleUrls: ['./add-page.page.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AddPagePage implements OnInit {


  ngOnInit() {
  }


  //#region PUBLIC MEMBERS

  //Represents the model which controls the start/stop scan button visibility.
  public showBLEScan = true;

  //Represents the scan process status.
  public searched = false;

  //Represents the device model which serves as a data source device list
  public devices: any[] = [];

  // formData = "*11&-700#*12&-065#*13&0000#*14&0001#*15&0002#*16&0000#*21&064#*22&-062#*23&0000#*24&0001#*25&0002#*26&0000#*30&CAT001#*01#$/";

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public diagnostic: Diagnostic,
    public toastProvider: ToastProvider,
    public platform: Platform,
    public LoadingProvider: LoadingProvider,
    public ble: BLE,
    public ngZone: NgZone) {

  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the device selected handler.
  deviceSelected(device) {

    //stop the scan.
    this.ble.stopScan();

    //route to the Load device data page. 
    this.navCtrl.navigateRoot('LoadDevicePage', { queryParams: { device: device } });
  }

  //#endregion

  //#region CLICK HANDLER

  //Represents the click handler for the start scan button.
  startScanClickHandler() {

    //start scanning the device.
    this.searchDevices();

    //Request for the bluetooth permission.
    // this.requestForBluetoothPermission();
  }

  //Represents the click handler for the stop scan button.
  stopBLEScanClicked() {

    //stop the scanning.
    this.ble.stopScan();

    //reset the UI.
    this.resetUIVariables();
  }

  
  //#endregion

  //#region CALLBACK HANDLERS

  //Represents the device discovered callback
  onDeviceDiscovered(device) {

    this.ngZone.run(() => {

      //discovered device.
      const derivedName = device.advertising.kCBAdvDataLocalName || device.name || 'unnamed';

      //if the device starts with CA then add it to the list.
      if (derivedName.startsWith('CA')) {

        //push the device to the model so that the UI will be updated.
        this.devices.push(device);
      }
    });
  }

  //Represents the scan error handler.
  onScanError(error) {

    this.ngZone.run(() => {

      //alert the error.
      alert("Scan error : " + JSON.stringify(error));

      //reset the UI.
      this.resetUIVariables();
    });
  }

  //#endregion

  //#region public METHODS

  public searchDevices() {

    //show the loader.
    this.LoadingProvider.showLoaderWithoutTimer();

    //reset status varibales.
    this.searched = false;

    //reset the devices model.
    this.devices = [];

    //hide the start scan button and show stop scan button.
    this.showBLEScan = false;

    //start the BLE scan.
    this.ble.scan([], 5).subscribe(device => this.onDeviceDiscovered(device), error => this.onScanError(error));

    //start timer to stop the scanning after 5 seconds. 
    setTimeout(function () {
      this.stopScanHandler();
    }.bind(this), 5000);
  }

  public requestForBluetoothPermission() {

    if (this.platform.is('android')) {

      //check for the android bluetooth permission
      this.checkForAndroidBluetoothPermission();
    }
    else {

      //check for the IOS bluetooth permission
      this.requestIOSBluetoothPermission();
    }
  }

  public checkForAndroidBluetoothPermission() {

    let self = this;

    //check if bluetooth is available on the device.
    if (!self.diagnostic.isBluetoothAvailable()) {

      self.toastProvider.present('Bluetooth is not available on this device');
    }
    else {

      //check if bluetooth is enabled or not.
      return self.diagnostic.isBluetoothEnabled().then(enabled => {

        //if bluetooth is turned off.
        if (!enabled) {

          //turn on the bluetooth.
          self.diagnostic.setBluetoothState(true).then((data) => {
            self.searchDevices();
          })
        }
        else {
          self.searchDevices();
        }
      }).catch(rejected => {
        self.toastProvider.present("Error occured while bluetooth" + rejected);
      });
    }
  }

  public requestIOSBluetoothPermission() {

    let self = this;

    //request for the bluetooth permission.
    // TODO : HIDE unhide later
    // cordova.plugins.BluProvisionWrapper.bluetoothPermission((err) => {

    //   this.ngZone.run(() => {

    //     //handle the bluetooth permission error.
    //     self.toastProvider.present(err.result);
    //   });
    // },
    //   (msg) => {

    //     this.ngZone.run(() => {
    //       self.searchDevices();
    //     });
    //   });
    // TODO : HIDE End
  }

  public stopScanHandler() {

    //reset the UI.
    this.resetUIVariables();
  }

  public resetUIVariables() {

    //hide loader.
    this.LoadingProvider.hideLoader();

    //reset status varibales.
    this.searched = false;

    //show start scan button and hide stop scan button.
    this.showBLEScan = true;
  }

  //#endregion


}
   