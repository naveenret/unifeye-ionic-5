import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/providers/user/user';
import { ProjectProvider } from 'src/app/providers/project/project';
import { Api } from 'src/app/providers/api/api';
import { AutoLoginProvider } from 'src/app/providers/autologin/autologin';
import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.page.html',
  styleUrls: ['./accounts.page.scss'],
})
export class AccountsPage implements OnInit {

  
  userInfo: any;
  selectedApi: any;

  ngOnInit(){}
  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public translateService: TranslateService,
    public user: User,
    public projectProvider: ProjectProvider,
    public api: Api,
    public autoLoginProvider: AutoLoginProvider) {
      this.userInfo = user.getUserInfo();
      console.log({user});
      console.log(this.userInfo);
      console.log(this.userInfo!.username);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BluAccountPage');
  }

  ionViewWillEnter() {
    let selectedApiUrl = this.api.getUrl();
    if(selectedApiUrl === this.api.getProdUrl()) {
      this.selectedApi = "prod";
    } else if(selectedApiUrl === this.api.getQaUrl()) {
      this.selectedApi = "qa";
    } else if(selectedApiUrl === this.api.getDevUrl()) {
      this.selectedApi = "dev";
    }
  }

  logoutClicked() {
    Auth.signOut()
          .then(data => console.log(data))
          .catch(err => console.log(err));
    this.user.logout();
    //this.projectProvider.setSite(undefined);
    this.autoLoginProvider.clearAllCredentials(this.logoutCallback.bind(this), this.logoutErrorCallback.bind(this));
  }

  logoutCallback() {
    this.navCtrl.navigateRoot('login');
  }
  
  logoutErrorCallback() {
    this.navCtrl.navigateRoot('login');
  }

  setApiDev() {
    this.showApiChangeAlert("Dev")
  }

  setApiQa() {
  this.showApiChangeAlert("Qa")
  }

  setApiProd() {
    this.showApiChangeAlert("Prod")
  }

  async showApiChangeAlert(apiName: String) {
    const prompt =await this.alertCtrl.create({
      header: 'Confirm Api Change',
      subHeader: 'Are you sure you want to change to ' + apiName + ' Api',
      buttons: [
        {
          text: this.translateService.instant('OK'),
          handler: data => {
            if(apiName === "Prod"){
              this.api.setUrlToProd();
            } else if(apiName === "Qa") {
              this.api.setUrlToQa()
            } else {
              this.api.setUrlToDev()
            }
            this.logoutClicked();
          }
        }
      ]
    });
    await prompt.present();
  }

}
