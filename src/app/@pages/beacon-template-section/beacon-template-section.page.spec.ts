import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeaconTemplateSectionPage } from './beacon-template-section.page';

describe('BeaconTemplateSectionPage', () => {
  let component: BeaconTemplateSectionPage;
  let fixture: ComponentFixture<BeaconTemplateSectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeaconTemplateSectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeaconTemplateSectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
