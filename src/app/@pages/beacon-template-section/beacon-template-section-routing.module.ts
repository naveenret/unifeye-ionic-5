import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeaconTemplateSectionPage } from './beacon-template-section.page';

const routes: Routes = [
  {
    path: '',
    component: BeaconTemplateSectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeaconTemplateSectionPageRoutingModule {}
