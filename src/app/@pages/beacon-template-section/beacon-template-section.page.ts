import { Component, OnInit } from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Beacon } from 'src/app/models/Beacon';
import { NavController } from '@ionic/angular';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-beacon-template-section',
  templateUrl: './beacon-template-section.page.html',
  styleUrls: ['./beacon-template-section.page.scss'],
})
export class BeaconTemplateSectionPage implements OnInit {

 
  //#region PUBLIC MEMBER

  //represents the template list used as a datasource.
  templates: Array<Template> = [];

  //Represents the currently selected beacon passed from the beacon provision page.
  beacon: Beacon;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public activateRoute: ActivatedRoute,
    public provisioningProvider: ProvisioningProvider) {

    this.provisioningProvider.setNav(navCtrl);
    
    //set the beacon passed from the beacon provision page.

    new Promise((resolve)=>{

      this.activateRoute.queryParams.subscribe(res=>{
        if(res && res.beacon){
          this.beacon = res.beacon;
        }
      })
      resolve();
    }).then(()=>{
      //load the beacon templates.
      this.loadTemplates();
    })


  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the template selected event.
  onTemplateSelected(template) {

    //prepare the route param model.
    let data = {
      beacon: this.beacon,
      template: template
    };

    //redirect to the Beacon Input page.
    this.navCtrl.navigateForward('beacon-input',{ queryParams: data});
  }

  //#endregion

  //#region PRIVATE METHODS

  //load the templates from the blu-vision plugin
  private loadTemplates() {

    let self = this;

    //pass the selected beacon id as a params.
    let param = self.beacon.id.toString();

    //load the templates for a device.
    this.provisioningProvider.loadTemplates(param, this.handleTemplateLoadedEvent.bind(this));
  }

  //handle the template loaded event.
  private handleTemplateLoadedEvent(templates: Array<Template>) {

    if (templates) {
      templates = templates.filter((x:any) => x.name === "Default Beacon Template");
      templates.forEach((x:any)=> x.name = x.name.replace(/Beacon/gi, 'Transmitter'));
    }

    //Update the template list to update the ui.
    this.templates = templates;
  }

  //#endregion

  //#region IMPLEMENTATION METHOD


  //#endregion


  ngOnInit() {
  }

}
