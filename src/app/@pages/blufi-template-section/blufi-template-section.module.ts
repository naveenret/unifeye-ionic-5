import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlufiTemplateSectionPageRoutingModule } from './blufi-template-section-routing.module';

import { BlufiTemplateSectionPage } from './blufi-template-section.page';
import { ComponentsModule } from 'src/app/@components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlufiTemplateSectionPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule.forChild(),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [BlufiTemplateSectionPage]
})
export class BlufiTemplateSectionPageModule {}
