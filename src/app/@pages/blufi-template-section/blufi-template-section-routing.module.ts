import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlufiTemplateSectionPage } from './blufi-template-section.page';

const routes: Routes = [
  {
    path: '',
    component: BlufiTemplateSectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlufiTemplateSectionPageRoutingModule {}
