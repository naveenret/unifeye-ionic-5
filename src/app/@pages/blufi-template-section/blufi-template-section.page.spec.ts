import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlufiTemplateSectionPage } from './blufi-template-section.page';

describe('BlufiTemplateSectionPage', () => {
  let component: BlufiTemplateSectionPage;
  let fixture: ComponentFixture<BlufiTemplateSectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlufiTemplateSectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlufiTemplateSectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
