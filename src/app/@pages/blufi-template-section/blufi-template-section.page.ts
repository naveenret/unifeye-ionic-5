import { Component, OnInit } from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Blufi } from 'src/app/models/Blufi';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ProvisioningProvider } from 'src/app/providers/provisioning/provisioning';

@Component({
  selector: 'app-blufi-template-section',
  templateUrl: './blufi-template-section.page.html',
  styleUrls: ['./blufi-template-section.page.scss'],
})
export class BlufiTemplateSectionPage implements OnInit {


  //#region PUBLIC MEMBER

  //represents the template list used as a datasource.
  templates: Array<Template> = [];

  //Represents the currently selected blufi passed from the blufi provision page.
  blufi: Blufi;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public provisioningProvider: ProvisioningProvider) {

    this.provisioningProvider.setNav(navCtrl);

    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        if (res && res.blufi) {

          this.blufi = res.blufi;
        }
      })
      resolve();
    }).then(() => {
      //set the blufi passed from the blufi provision page.

      //load the beacon templates.
      this.loadTemplates();
    })
  }

  //#endregion

  //#region PUBLIC METHODS

  //Represents the template selected event.
  onTemplateSelected(template) {

    //prepare the route param model.
    let data = {
      blufi: this.blufi,
      template: template
    };

    this.navCtrl.navigateForward('blufi-input', { queryParams: data });
  }

  //#endregion

  //#region PRIVATE METHODS

  //load the templates from the blu-vision plugin
  private loadTemplates() {

    let self = this;

    //pass the selected blufi id as a params.
    let param = self.blufi.id.toString();

    //load the templates for a device.
    this.provisioningProvider.loadTemplates(param, this.handleTemplateLoadedEvent.bind(this));
  }

  //handle the template loaded event.
  private handleTemplateLoadedEvent(templates: Array<Template>) {

    //Update the template list to update the ui.
    this.templates = templates;
  }

  //#endregion

  //#region IMPLEMENTATION METHOD

  //#endregion
  ngOnInit() {
  }

}
