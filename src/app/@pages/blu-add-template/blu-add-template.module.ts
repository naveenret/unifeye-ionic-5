import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BluAddTemplatePageRoutingModule } from './blu-add-template-routing.module';

import { BluAddTemplatePage } from './blu-add-template.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BluAddTemplatePageRoutingModule,
    TranslateModule.forChild(),
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [BluAddTemplatePage],
  exports:[BluAddTemplatePage]
})
export class BluAddTemplatePageModule {}
