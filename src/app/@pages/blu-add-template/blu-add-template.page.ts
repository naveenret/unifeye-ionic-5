import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { TemplateProvider } from 'src/app/providers/templates/templates';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { ProjectProvider } from 'src/app/providers/project/project';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-blu-add-template',
  templateUrl: './blu-add-template.page.html',
  styleUrls: ['./blu-add-template.page.scss'],
})
export class BluAddTemplatePage implements OnInit {


  ngOnInit() {
  }
//#region PUBLIC MEMBER

selectedTemplateType = '';

//#endregion

//#region CONSTRUCTOR

constructor(public formBuilder: FormBuilder,
  public modalCtrl: ModalController,
  public templateProvider: TemplateProvider,
  public LoadingProvider: LoadingProvider,
  public toastProvider: ToastProvider,
  public projectProvider: ProjectProvider,
  public translateService: TranslateService) {
  this.selectedTemplateType = "gateway";
}

//#endregion

//#region PUBLIC METHODS

public async addTemplate() {
  let modal =await this.modalCtrl.create({
    component: BluAddTemplatePage
  });
  await modal.present();
}

public close() {
  this.modalCtrl.dismiss(false);
}

//#endregion

//#region PRIVATE METHODS
//#endregion
}
