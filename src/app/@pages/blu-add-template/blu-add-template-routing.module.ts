import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BluAddTemplatePage } from './blu-add-template.page';

const routes: Routes = [
  {
    path: '',
    component: BluAddTemplatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BluAddTemplatePageRoutingModule {}
