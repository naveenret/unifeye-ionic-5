import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BluAddTemplatePage } from './blu-add-template.page';

describe('BluAddTemplatePage', () => {
  let component: BluAddTemplatePage;
  let fixture: ComponentFixture<BluAddTemplatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluAddTemplatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BluAddTemplatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
