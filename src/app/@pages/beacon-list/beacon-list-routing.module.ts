import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeaconListPage } from './beacon-list.page';

const routes: Routes = [
  {
    path: '',
    component: BeaconListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeaconListPageRoutingModule {}
