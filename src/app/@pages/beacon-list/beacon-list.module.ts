import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeaconListPageRoutingModule } from './beacon-list-routing.module';

import { BeaconListPage } from './beacon-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/@components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeaconListPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    ComponentsModule
  ],
  declarations: [BeaconListPage]
})
export class BeaconListPageModule {}
