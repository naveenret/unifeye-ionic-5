import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeaconListPage } from './beacon-list.page';

describe('BeaconListPage', () => {
  let component: BeaconListPage;
  let fixture: ComponentFixture<BeaconListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeaconListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeaconListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
