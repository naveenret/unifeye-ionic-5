import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastProvider } from 'src/app/providers/toast/toast';
import { LoadingProvider } from 'src/app/providers/loading/loading';
import { AlertController, NavController } from '@ionic/angular';
import { BeaconProvider } from 'src/app/providers/beacon/beacon';

@Component({
  selector: 'app-beacon-list',
  templateUrl: './beacon-list.page.html',
  styleUrls: ['./beacon-list.page.scss'],
})
export class BeaconListPage implements OnInit {


  isSearchBarOpened =false;
  //#region PUBLIC MEMBERS
  //Represents the beacon list model.
  beacons: Array<any>;
  filteredBeacons: Array<any>;

  //Represents the site id for which the beacon needs to be loaded
  siteId: Number;

  //#endregion

  //#region CONSTRUCTOR

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public beaconProvider: BeaconProvider,
    public LoadingProvider: LoadingProvider,
    public toastProvider: ToastProvider,
    public translateService: TranslateService,
    public activatedRoute: ActivatedRoute) {
    //get the site id from the params.


  }

  ngOnInit() {
    debugger;
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        this.siteId = res && res!.siteId || '';
      })
      resolve();
    }).then(() => {
      //load the alerts for a give site.
      if (this.siteId === 0) {
        this.loadAllBeaconList();
      } else {
        this.loadBeaconList();
      }
    })
    console.log('this.siteId', this.siteId);
  }


  private initializeItems(): void {
    this.filteredBeacons = this.beacons;
  }

  goToBulkBeaconUploads() {
    this.navCtrl.navigateForward('blu-bulk-upload-beacons-page', { queryParams: { siteId: this.siteId } });
  }

  getItems(evt) {
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.filteredBeacons = this.filteredBeacons.filter(currentGoal => {
      let result = false;
      if (currentGoal.name) {
        if (currentGoal.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          result = true;
        }
      }
      if (currentGoal.deviceId) {
        if (currentGoal.deviceId.indexOf(searchTerm) > -1) {
          result = true;
        }
      }
      return result;
    });
  }

  //#endregion

  //#region PRIVATE METHODS

  //load the beacons for a site.
  private loadBeaconList() {

    //show loader.
    this.LoadingProvider.showLoader();

    //load beacons for a site
    this.beaconProvider.loadBeaconList(this.siteId).subscribe((beacons: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the beacons model.
      this.beacons = beacons;
      this.filteredBeacons = beacons;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  //#endregion

  private loadAllBeaconList() {

    //show loader.
    this.LoadingProvider.showLoader();

    //load beacons for a site
    this.beaconProvider.loadAllBeaconList(this.siteId).subscribe((beacons: Array<any>) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //set the beacons model.
      this.beacons = beacons;
      this.filteredBeacons = beacons;
    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      if (error.status !== 401) {
        this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
      }
    });
  }

  public async deleteBeacon(bID) {
    if (this.siteId !== 0) {
      this.deleteBeacons(bID);
    }
    else {
      const prompt = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Cannot delete beacon from currently selected (All) location. Please change your location from dashboard.',
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('OK'),
            handler: data => {
              this.navCtrl.navigateRoot('tabs');
            }
          }
        ]
      });
      await prompt.present();
    }
  }

  private deleteBeacons(bID) {

    this.LoadingProvider.showLoader();

    this.beaconProvider.deleteBeacons(this.siteId, [bID]).subscribe(() => {

      this.beacons = this.beacons.filter((x) => x.deviceId !== bID);
      this.filteredBeacons = this.beacons;
      //hide the loader.
      this.LoadingProvider.hideLoader();

      this.toastProvider.present('Successfully Deleted');

    }, (error) => {

      //hide the loader.
      this.LoadingProvider.hideLoader();

      //display error message.
      this.toastProvider.present(this.translateService.instant('SERVER_ERROR'));
    });
  }

  openDetails(beacon: any) {
    this.navCtrl.navigateForward('beacon-detail', { queryParams: { selectedBeacon: beacon, siteId: this.siteId } });
  }

  ionViewWillEnter() {
    //load the beacons for a give site.
    
  }

}
