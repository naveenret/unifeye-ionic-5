export class util {

  public static replaceWithDigit(tempString:string):string {
    let temperature = tempString;
    temperature = temperature.substring(4, temperature.length);
    console.log('temperature ', temperature);
    if(temperature.charAt(3) === '0') {
      temperature = temperature.substring(0,3) + '.' + temperature.charAt(4);
    }

    return temperature;
  }

  private static padStart(inputString, targetLength, padString) {
    targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
    padString = String(typeof padString !== 'undefined' ? padString : ' ');
    if (inputString.length >= targetLength) {
      return String(inputString);
    } else {
      targetLength = targetLength - inputString.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return padString.slice(0, targetLength) + String(inputString);
    }
  }

  public static bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

  public static bytesToStringUnit16(buffer) {
    return String.fromCharCode.apply(null, new Uint16Array(buffer));
  }

  public static checksumGenerator(formdataStr) {

    let checksumStr = 0;

    const formdataArr = formdataStr.split('');

    for(let i = 0; i < formdataArr.length; i++) {
      checksumStr = checksumStr + ( formdataStr.charCodeAt(i)- 48);
    }

    // alert("before adding 48 : " + checksumStr);

    checksumStr = checksumStr + 48;
    
    // alert("after adding 48 : " + checksumStr);
    

    if (checksumStr > -129) {
      // alert("Inside > 127  : " + checksumStr);
      checksumStr -= 127;
      // alert("After > 127  : " + checksumStr);
    }

    console.log('checksum string: ', String.fromCharCode(checksumStr));

    return String.fromCharCode(checksumStr);
  }

  // ASCII only
  public static stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
      array[i] = string.charCodeAt(i);
    }

    // console.log('array size', array.length);

    return array.buffer;
  }

  public static _transformToString(formData: Array<string>): string {

    let formDataStr = formData.toString().replace(/,/g, '');
    formDataStr = formDataStr.concat(util.checksumGenerator(formDataStr).toString());

    const size = 106 - formDataStr.length;

    for(var index = 0; index < size; index++) {
      formDataStr = formDataStr.concat('0');
    }

    return formDataStr;

  }

  public static formatTemperature(temp: string): string {
    const tempFloat = parseFloat(temp).toFixed(1);
    console.log('temp format ', this.padStart(tempFloat.toString().replace('.', '0'),5,'0'));
    return this.padStart(tempFloat.toString().replace('.', '0'),5,'0');
  }

  public static getDateValues(): string {
    let currentDate = new Date();

    const data = ("0" + (currentDate.getUTCMonth() + 1)).slice(-2)
      + ("0" + currentDate.getUTCDate()).slice(-2)
      + currentDate.getUTCFullYear().toString().substring(2,4);

    return data;

  }

  public static getTimeValues(): string {
    let currentDate = new Date();

    const data = ("0" + (currentDate.getUTCHours() + 1)).slice(-2)
      + ("0" + currentDate.getUTCMinutes()).slice(-2)
      + ("0" + currentDate.getUTCSeconds()).slice(-2)

    return data;

  }
}
