// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // API url
  _API_URL: 'https://u2n0byo4xl.execute-api.us-east-1.amazonaws.com/uat/api',

  // REQUIRED - Amazon Cognito Region
  region: 'us-east-1',

  // OPTIONAL - Amazon Cognito User Pool ID
  userPoolId: 'us-east-1_LYZWLMRc8',//'us-east-1_X86mpkgvA',

  // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
  userPoolWebClientId: '49lrvh1k6t4sopledrmat63ki6', //'m2up0j6tbsn8k27ej754gtdpc',

  // scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],

  domain: 'https://dcctest.auth.us-east-1.amazoncognito.com',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
