import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import Amplify, { Auth } from 'aws-amplify';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));


Amplify.configure({
  Auth: {

    // REQUIRED - Amazon Cognito Region
    region: environment.region,

    // OPTIONAL - Amazon Cognito User Pool ID
    userPoolId: environment.userPoolId,//'us-east-1_X86mpkgvA',

    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    userPoolWebClientId: environment.userPoolWebClientId, //'m2up0j6tbsn8k27ej754gtdpc',

    // scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],

    oauth: {
      domain: environment.domain,
      // scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
      redirectSignIn: 'http://localhost:8100',
      redirectSignOut: 'http://localhost:8100',
      responseType: 'token' // or 'token', note that REFRESH token will only be generated when the responseType is code
    }

  }
});