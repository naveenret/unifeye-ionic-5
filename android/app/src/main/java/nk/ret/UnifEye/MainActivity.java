package nk.ret.UnifEye;

import android.os.Bundle;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;

import java.util.ArrayList;
import io.stewan.capacitor.analytics.AnalyticsPlugin;
import io.stewan.capacitor.crashlytics.CrashlyticsPlugin;
public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
      add(AnalyticsPlugin.class);

      try {
        //  Block of code to try
      add(CrashlyticsPlugin.class);
        System.out.println("CrashlyticsPlugin added");

      }
      catch(Exception e) {
        //  Block of code to handle errors
        System.out.println("CrashlyticsPlugin add error"+e);
      }
    }});
  }
}
